% check HDDM input data distributions (MRI and EEG superimposed)

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;
pn.plotFolder = fullfile(pn.root, 'figures');

load(fullfile(pn.root, 'data', 'HDDM_summary_YA_vat.mat'))

EEGInput = load(fullfile(pn.root, 'data', 'StateSwitchDynamicTrialData_EEG_YA.mat'));
MRIInput = load(fullfile(pn.root, 'data', 'StateSwitchDynamicTrialData_MRI_YA.mat'));

commonIDs = unique(EEGInput.data_YA(:,7));

DDMIDidx = ismember(cell2mat(cellfun(@str2num, HDDM_summary.IDs, 'un', 0)),EEGInput.data_YA(:,7));
DDMIDidx = find(DDMIDidx);

countsEEG = []; countsMRI = [];
edges = 0:.05:2;
for indID = 1:numel(commonIDs)
    for indCond = 1:4
        countsEEG(indID,indCond,:) = histcounts(EEGInput.data_YA(EEGInput.data_YA(:,7) == commonIDs(indID) & EEGInput.data_YA(:,4) == indCond,3),edges);
    end
end

%% Figure SXX: sort by DDM-estimated NDT

conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};

h = figure('units','normalized','position',[.1 .1 .4 .4]);
indCount = 1;
for indCond = 1:3:4
    subplot(1,2,indCount)
    if indCond == 1
        [~, sortIdx] = sort(HDDM_summary.nondecisionEEG(DDMIDidx));
    end
    imagesc(edges, [], squeeze(countsEEG(sortIdx,indCond,:)))
    hold on; plot(HDDM_summary.nondecisionEEG(DDMIDidx(sortIdx),1),1:47, 'red', 'LineWidth', 4, 'LineStyle', ':')
    hold on; plot(HDDM_summary.nondecisionEEG(DDMIDidx(sortIdx),indCond),1:47, 'white', 'LineWidth', 4)
    title(['RT distributions ', conds{indCond}])
    xlabel('RT bin (s)'); ylabel('Subjects (sorted by NDT in Load 1)')
    indCount = indCount+1;
    cb = colorbar('location', 'SouthOutside'); set(get(cb,'ylabel'),'string','Amount of responses');
end
%suptitle('DDM: Individual RT distributions sorted by NDT in Load 1')
colormap('hot')
set(findall(gcf,'-property','FontSize'),'FontSize',20)

figureName = 'A3_NDT_RTdistribution';
saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% replicate for OAs

EEGInput = load(fullfile(pn.root, 'data', 'StateSwitchDynamicTrialData_EEG_OA.mat'));
MRIInput = load(fullfile(pn.root, 'data', 'StateSwitchDynamicTrialData_MRI_OA.mat'));

load(fullfile(pn.root, 'data', 'HDDM_summary_OA_vat.mat'))

commonIDs = unique(EEGInput.data_OA(:,7));

DDMIDidx = ismember(cell2mat(cellfun(@str2num, HDDM_summary.IDs, 'un', 0)),EEGInput.data_OA(:,7));
DDMIDidx = find(DDMIDidx);

countsEEG = []; countsMRI = [];
edges = 0:.05:2;
for indID = 1:numel(commonIDs)
    for indCond = 1:4
        countsEEG(indID,indCond,:) = histcounts(EEGInput.data_OA(EEGInput.data_OA(:,7) == commonIDs(indID) & EEGInput.data_OA(:,4) == indCond,3),edges);
    end
end

%% Figure SXX: sort by DDM-estimated NDT

conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};

h = figure('units','normalized','position',[.1 .1 .4 .4]);
indCount = 1;
for indCond = 1:3:4
    subplot(1,2,indCount)
    if indCond == 1
        [~, sortIdx] = sort(HDDM_summary.nondecisionEEG(DDMIDidx));
    end
    imagesc(edges, [], squeeze(countsEEG(sortIdx,indCond,:)))
    hold on; plot(HDDM_summary.nondecisionEEG(DDMIDidx(sortIdx),1),1:numel(sortIdx), 'red', 'LineWidth', 4, 'LineStyle', ':')
    hold on; plot(HDDM_summary.nondecisionEEG(DDMIDidx(sortIdx),indCond),1:numel(sortIdx), 'white', 'LineWidth', 4)
    title(['RT distributions ', conds{indCond}])
    xlabel('RT bin (s)'); ylabel('Subjects (sorted by NDT in Load 1)')
    indCount = indCount+1;
    cb = colorbar('location', 'SouthOutside'); set(get(cb,'ylabel'),'string','Amount of responses');
end
%suptitle('DDM: Individual RT distributions sorted by NDT in Load 1')
colormap('hot')
set(findall(gcf,'-property','FontSize'),'FontSize',20)

figureName = 'A3_NDT_RTdistribution_OA';
saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');
