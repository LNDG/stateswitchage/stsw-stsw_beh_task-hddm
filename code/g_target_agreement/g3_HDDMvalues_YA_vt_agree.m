% Calculate reliability of HDDM estimates

% load data as table, convert to array
pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/';
addpath(genpath([pn.root, 'T_tools/']));
dataFile = [pn.root, 'B_data/EEGAgree/m_dim_agree_v_t.csv'];
tableData = readtable(dataFile, 'ReadRowNames', 1);
arrayData = table2array(tableData);
ColumnNames = tableData.Properties.VariableNames'; clear tableData;

% get individual subject data

params = {'v'; 'a'; 't'};
ageGroups = {'1'};
targets = {'1'; '2'; '3'; '4'};
convergence = {'1'; '2'; '3'; '4'; '5'; '6'};

Indices = []; EntryNum = []; MeanValuesEEG = [];
MeanValuesEEG = NaN(numel(params), numel(targets),numel(convergence), 102);
for indParam = 1:numel(params)
    for indAge = 1:numel(ageGroups)
        for indCond = 1:numel(targets)
            for indConvergence = 1:numel(convergence)
                if strcmp(params{indParam}, 'a')
                    strPattern=[params{indParam}, '_subj_'];
                else
                    strPattern=[params{indParam}, '_subj_',convergence{indConvergence},'_',targets{indCond},'_'];
                end
                Indices = find(contains(ColumnNames,strPattern));
                EntryNum{indAge,indParam,indCond,indConvergence} = ...
                    strrep(strrep(cellfun(@(x) x(end-2:end),ColumnNames(Indices),...
                    'un',false), '_', ''),'j', '');
                EntryNum{indAge,indParam,indCond,indConvergence} = cell2mat(cellfun(@str2num,EntryNum{indAge,indParam,indCond,indConvergence},'un',0));
                MeanValuesEEG(indParam,indCond,indConvergence,EntryNum{indAge,indParam,indCond,indConvergence}) = nanmean(arrayData(:,Indices),1);
            end
        end
    end
end
clear EntryNum Indices

% subjects are indexed with respect to IDs_all
pn.dataIn = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/';
load([pn.dataIn, 'SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat'],'IDs_all');

idx_YA = cellfun(@str2num, IDs_all)<2000;
idx_OA = cellfun(@str2num, IDs_all)>2000;

%% plot mean effects using RainCloudPlots
 
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/B_reliabilityAcrossSessions/D_tools/'))

condPairs = [1,2; 2,3; 3,4];
condPairsLevel = [];
condPairsLevel{1} = [5,2.5, 2.2];  
condPairsLevel{2} = [1.6, 1.75, 2];  
condPairsLevel{3} = [.45, .5, .55]; 
lims{1} = [-.5 5.5]; lims{2} = [-2 6]; lims{3} = [0 .6];
%colorm = [43/255 140/255 190/255; 230/255 75/255 50/255; 230/255 75/255 50/255];
colorm = [0.0314, 0.3176, 0.6118; .5,.5,.5; 2.*[.3 .1 .1]]
paramLabels = {'Drift rate'; 'Threshold'; 'Non-decision time'}; 

h = figure('units','normalized','position',[.1 .1 .15 .6]);
for indIdx = [1,3]
    subplot(3,1,indIdx)
    cla; hold on;

    curData = squeeze(permute(nanmean(MeanValuesEEG(indIdx,:,1:6,idx_YA),3),[4,1,2,3]));
    
 % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = squeeze(curData(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = curData(:,i)-...
                nanmean(curData(:,:),2)+...
                repmat(nanmean(nanmean(curData(:,:),2),1),size(curData(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = colorm(indIdx,:);

    box off
    cla;
        h_rc = rm_raincloud(data_ws, cl,1);
        % add stats
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
               mysigstar_vert(gca, [condPairsLevel{indIdx}(indPair), condPairsLevel{indIdx}(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

            end
        end
        view([90 -90]);
        axis ij
    box(gca,'off')
    %set(gca, 'YTick', [1,2,3,4]);
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    ylabel('Target load'); xlabel(paramLabels{indIdx})
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    xlim(lims{indIdx}); 
%     curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);
    
    X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes_load(indIdx,:) = b(2,:);
end

%% plot effects by convergence

condPairs = [1,2; 2,3; 3,4; 4,5; 5,6];
condPairsLevel = [];
condPairsLevel{1} = fliplr([2,2,2, 2, 2]);  
condPairsLevel{2} = fliplr([1.6, 1.75, 2,2,2]);   
condPairsLevel{3} = fliplr([.45, .5, .55, .5, .5]);  
lims{1} = [0 2.5]; lims{2} = [-2 6]; lims{3} = [.1 .6];
%colorm = [43/255 140/255 190/255; 230/255 75/255 50/255; 230/255 75/255 50/255];
colorm = [0.0314, 0.3176, 0.6118; .5,.5,.5; 2.*[.3 .1 .1]]
paramLabels = {'Drift rate'; 'Threshold'; 'Non-decision time'}; 

h = figure('units','normalized','position',[.1 .1 .15 .6]);
set(0, 'DefaultFigureRenderer', 'painters'); hold on;
for indIdx = [1,3]%1:3
    subplot(3,1,indIdx)
    cla; hold on;

    curData = fliplr(squeeze(permute(nanmean(MeanValuesEEG(indIdx,1:4,1:6,idx_YA),2),[4,1,3,2])));
    
 % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:size(curData,2)
        for j = 1:1
            data{i, j} = squeeze(curData(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = curData(:,i)-...
                nanmean(curData(:,:),2)+...
                repmat(nanmean(nanmean(curData(:,:),2),1),size(curData(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = colorm(indIdx,:);

    box off
    cla;
        h_rc = rm_raincloud(data_ws, cl,1);
        % add stats
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
               mysigstar_vert(gca, [condPairsLevel{indIdx}(indPair), condPairsLevel{indIdx}(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

            end
        end
        view([90 -90]);
        axis ij
    box(gca,'off')
    %set(gca, 'YTick', [1,2,3,4,5]);
    set(gca, 'YTickLabels', {'1/4';'1/3'; '1/2'; '2/3'; '3/4'; '1'});
    ylabel('Convergence'); xlabel(paramLabels{indIdx})
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    xlim(lims{indIdx}); 
    curYTick = get(gca, 'YTick'); ylim([curYTick(1)-1*(curYTick(2)-curYTick(1)) curYTick(end)+1*(curYTick(2)-curYTick(1))]);

    X = [1 1; 1 2; 1 3; 1 4; 1 5; 1 6]; b=X\curData'; IndividualSlopes_agree(indIdx,:) = b(2,:);
    [~, p(indIdx), ci, stats] = ttest(IndividualSlopes_agree(indIdx,:));
    title(['linear:', num2str(round(p(indIdx), 3))])
end

% figure; scatter(IndividualSlopes_load(1,:), IndividualSlopes_agree(1,:), 'filled')
% [r,p] = corrcoef(IndividualSlopes_load(1,:), IndividualSlopes_agree(1,:))

set(findall(gcf,'-property','FontSize'),'FontSize',22)
pn.plotFolder = [pn.root, 'C_figures/'];
figureName = 'G_meanEffects_EEG_YA_vt_agree_RCP';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% SourceData

SourceData_Drift = fliplr(squeeze(permute(nanmean(MeanValuesEEG(1,1:4,1:6,idx_YA),2),[4,1,3,2])));
SourceData_Drift(isnan(SourceData_Drift(:,1)),:) = [];

SourceData_NDT = fliplr(squeeze(permute(nanmean(MeanValuesEEG(3,1:4,1:6,idx_YA),2),[4,1,3,2])));
SourceData_NDT(isnan(SourceData_NDT(:,1)),:) = [];

%% get MRI-based DDM values to investigate reliability

% load data as table, convert to array
pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/';
addpath(genpath([pn.root, 'T_tools/']));
dataFile = [pn.root, 'B_data/MRIAgree/m_dim_agree_v_t.csv'];
tableData = readtable(dataFile, 'ReadRowNames', 1);
arrayData = table2array(tableData);
ColumnNames = tableData.Properties.VariableNames'; clear tableData;

% get individual subject data

params = {'v'; 'a'; 't'};
ageGroups = {'1'};
targets = {'1'; '2'; '3'; '4'};
convergence = {'1'; '2'; '3'; '4'; '5'; '6'};

Indices = []; EntryNum = []; MeanValuesMRI = [];
MeanValuesMRI = NaN(numel(params), numel(targets),numel(convergence), 102);
for indParam = 1:numel(params)
    for indAge = 1:numel(ageGroups)
        for indCond = 1:numel(targets)
            for indConvergence = 1:numel(convergence)
                if strcmp(params{indParam}, 'a')
                    strPattern=[params{indParam}, '_subj_'];
                else
                    strPattern=[params{indParam}, '_subj_',convergence{indConvergence},'_',targets{indCond},'_'];
                end
                Indices = find(contains(ColumnNames,strPattern));
                EntryNum{indAge,indParam,indCond,indConvergence} = ...
                    strrep(strrep(cellfun(@(x) x(end-2:end),ColumnNames(Indices),...
                    'un',false), '_', ''),'j', '');
                EntryNum{indAge,indParam,indCond,indConvergence} = cell2mat(cellfun(@str2num,EntryNum{indAge,indParam,indCond,indConvergence},'un',0));
                MeanValuesMRI(indParam,indCond,indConvergence,EntryNum{indAge,indParam,indCond,indConvergence}) = nanmean(arrayData(:,Indices),1);
            end
        end
    end
end
clear EntryNum Indices

% subjects are indexed with respect to IDs_all
pn.dataIn = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/';
load([pn.dataIn, 'SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat'],'IDs_all');

idx_YA = cellfun(@str2num, IDs_all)<2000;
idx_OA = cellfun(@str2num, IDs_all)>2000;

%% plot mean effects using RainCloudPlots
 
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/B_reliabilityAcrossSessions/D_tools/'))

condPairs = [1,2; 2,3; 3,4];
condPairsLevel = [];
condPairsLevel{1} = [5,2.5, 2.2];  
condPairsLevel{2} = [1.6, 1.75, 2];  
condPairsLevel{3} = [.45, .5, .55]; 
lims{1} = [-.5 5.5]; lims{2} = [-2 6]; lims{3} = [0 .6];
%colorm = [43/255 140/255 190/255; 230/255 75/255 50/255; 230/255 75/255 50/255];
colorm = [0.0314, 0.3176, 0.6118; .5,.5,.5; 2.*[.3 .1 .1]]
paramLabels = {'Drift rate'; 'Threshold'; 'Non-decision time'}; 

h = figure('units','normalized','position',[.1 .1 .15 .6]);
for indIdx = [1,3]
    subplot(3,1,indIdx)
    cla; hold on;

    curData = squeeze(permute(nanmean(MeanValuesMRI(indIdx,:,1:6,idx_YA),3),[4,1,2,3]));
    
 % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = squeeze(curData(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = curData(:,i)-...
                nanmean(curData(:,:),2)+...
                repmat(nanmean(nanmean(curData(:,:),2),1),size(curData(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = colorm(indIdx,:);

    box off
    cla;
        h_rc = rm_raincloud(data_ws, cl,1);
        % add stats
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
               mysigstar_vert(gca, [condPairsLevel{indIdx}(indPair), condPairsLevel{indIdx}(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

            end
        end
        view([90 -90]);
        axis ij
    box(gca,'off')
    %set(gca, 'YTick', [1,2,3,4]);
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    ylabel('Target load'); xlabel(paramLabels{indIdx})
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    xlim(lims{indIdx}); 
    curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);
    
    X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes_load(indIdx,:) = b(2,:);
end

%% plot effects by convergence

condPairs = [1,2; 2,3; 3,4; 4,5; 5,6];
condPairsLevel = [];
condPairsLevel{1} = fliplr([2,2,2, 2, 2]);  
condPairsLevel{2} = fliplr([1.6, 1.75, 2,2,2]);   
condPairsLevel{3} = fliplr([.45, .5, .55, .5, .5]);  
lims{1} = [0 2.5]; lims{2} = [-2 6]; lims{3} = [.2 .7];
%colorm = [43/255 140/255 190/255; 230/255 75/255 50/255; 230/255 75/255 50/255];
colorm = [0.0314, 0.3176, 0.6118; .5,.5,.5; 2.*[.3 .1 .1]]
paramLabels = {'Drift rate'; 'Threshold'; 'Non-decision time'}; 

h = figure('units','normalized','position',[.1 .1 .15 .6]);
set(0, 'DefaultFigureRenderer', 'painters'); hold on;
for indIdx = [1,3]%1:3
    subplot(3,1,indIdx)
    cla; hold on;

    curData = fliplr(squeeze(permute(nanmean(MeanValuesMRI(indIdx,1:4,1:6,idx_YA),2),[4,1,3,2])));
    
 % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:size(curData,2)
        for j = 1:1
            data{i, j} = squeeze(curData(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = curData(:,i)-...
                nanmean(curData(:,:),2)+...
                repmat(nanmean(nanmean(curData(:,:),2),1),size(curData(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = colorm(indIdx,:);

    box off
    cla;
        h_rc = rm_raincloud(data_ws, cl,1);
        % add stats
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
               mysigstar_vert(gca, [condPairsLevel{indIdx}(indPair), condPairsLevel{indIdx}(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

            end
        end
        view([90 -90]);
        axis ij
    box(gca,'off')
    %set(gca, 'YTick', [1,2,3,4,5]);
    set(gca, 'YTickLabels', {'1/4';'1/3'; '1/2'; '2/3'; '3/4'; '1'});
    ylabel('Convergence'); xlabel(paramLabels{indIdx})
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    xlim(lims{indIdx}); 
    curYTick = get(gca, 'YTick'); ylim([curYTick(1)-1*(curYTick(2)-curYTick(1)) curYTick(end)+1*(curYTick(2)-curYTick(1))]);

    X = [1 1; 1 2; 1 3; 1 4; 1 5; 1 6]; b=X\curData'; IndividualSlopes_agree(indIdx,:) = b(2,:);
    [~, p(indIdx), ci, stats] = ttest(IndividualSlopes_agree(indIdx,:));
    title(['linear:', num2str(round(p(indIdx), 3))])
end

% figure; scatter(IndividualSlopes_load(1,:), IndividualSlopes_agree(1,:), 'filled')
% [r,p] = corrcoef(IndividualSlopes_load(1,:), IndividualSlopes_agree(1,:))

set(findall(gcf,'-property','FontSize'),'FontSize',22)
pn.plotFolder = [pn.root, 'C_figures/'];
figureName = 'G_meanEffects_MRI_YA_vt_agree_RCP';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');


%% collect DDM estimates in a structure

HDDM_summary.thresholdMRI_load = squeeze(permute(nanmean(MeanValuesMRI(2,:,1:6,:),3),[4,1,2,3]));
HDDM_summary.nondecisionMRI_load = squeeze(permute(nanmean(MeanValuesMRI(3,:,1:6,:),3),[4,1,2,3]));
HDDM_summary.driftMRI_load = squeeze(permute(nanmean(MeanValuesMRI(1,:,1:6,:),3),[4,1,2,3]));

HDDM_summary.thresholdEEG_load = squeeze(permute(nanmean(MeanValuesEEG(2,:,1:6,:),3),[4,1,2,3]));
HDDM_summary.nondecisionEEG_load = squeeze(permute(nanmean(MeanValuesEEG(3,:,1:6,:),3),[4,1,2,3]));
HDDM_summary.driftEEG_load = squeeze(permute(nanmean(MeanValuesEEG(1,:,1:6,:),3),[4,1,2,3]));

HDDM_summary.thresholdMRI_agree = fliplr(squeeze(permute(nanmean(MeanValuesMRI(3,1:4,1:6,:),2),[4,1,3,2])));
HDDM_summary.nondecisionMRI_agree = fliplr(squeeze(permute(nanmean(MeanValuesMRI(2,1:4,1:6,:),2),[4,1,3,2])));
HDDM_summary.driftMRI_agree = fliplr(squeeze(permute(nanmean(MeanValuesMRI(1,1:4,1:6,:),2),[4,1,3,2])));

HDDM_summary.thresholdEEG_agree = fliplr(squeeze(permute(nanmean(MeanValuesEEG(3,1:4,1:6,:),2),[4,1,3,2])));
HDDM_summary.nondecisionEEG_agree = fliplr(squeeze(permute(nanmean(MeanValuesEEG(2,1:4,1:6,:),2),[4,1,3,2])));
HDDM_summary.driftEEG_agree = fliplr(squeeze(permute(nanmean(MeanValuesEEG(1,1:4,1:6,:),2),[4,1,3,2])));

HDDM_summary.IDs = IDs_all;

dataPath = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/';
save([dataPath, 'G_HDDM_summary_YA_vt_agree_dim.mat'], 'HDDM_summary')

%% correlate with original estimates of final model

origModel = load([dataPath, 'HDDM_summary_YA_vt.mat'], 'HDDM_summary');
agreeModel = load([dataPath, 'G_HDDM_summary_YA_vt_agree_dim.mat'], 'HDDM_summary');

% fit linear effects in both models

curData1 = origModel.HDDM_summary.driftEEG;
curData2 = agreeModel.HDDM_summary.driftEEG_load;

X = [1 1; 1 2; 1 3; 1 4]; b=X\curData1'; IndividualSlopes1 = b(2,:);
X = [1 1; 1 2; 1 3; 1 4]; b=X\curData2'; IndividualSlopes2 = b(2,:);

figure; 
% exclude NaNs within EEG/MRI session
x = IndividualSlopes1(find(~isnan(IndividualSlopes1(1:53))));
y = IndividualSlopes2(find(~isnan(IndividualSlopes1(1:53))));
scatter(x, y, 'filled'); [r, p] = corrcoef(x,y)

curData1 = origModel.HDDM_summary.driftMRI;
curData2 = agreeModel.HDDM_summary.driftMRI_load;

X = [1 1; 1 2; 1 3; 1 4]; b=X\curData1'; IndividualSlopes1 = b(2,:);
X = [1 1; 1 2; 1 3; 1 4]; b=X\curData2'; IndividualSlopes2 = b(2,:);

figure; 
x = IndividualSlopes1(find(~isnan(IndividualSlopes1(1:53))));
y = IndividualSlopes2(find(~isnan(IndividualSlopes1(1:53))));
scatter(x, y, 'filled'); [r, p] = corrcoef(x,y)

curData1 = origModel.HDDM_summary.nondecisionEEG;
curData2 = agreeModel.HDDM_summary.nondecisionEEG_load;

X = [1 1; 1 2; 1 3; 1 4]; b=X\curData1'; IndividualSlopes1 = b(2,:);
X = [1 1; 1 2; 1 3; 1 4]; b=X\curData2'; IndividualSlopes2 = b(2,:);

figure; 
x = IndividualSlopes1(find(~isnan(IndividualSlopes1(1:53))));
y = IndividualSlopes2(find(~isnan(IndividualSlopes1(1:53))));
scatter(x, y, 'filled'); [r, p] = corrcoef(x,y)

curData1 = origModel.HDDM_summary.nondecisionMRI;
curData2 = agreeModel.HDDM_summary.nondecisionMRI_load;

X = [1 1; 1 2; 1 3; 1 4]; b=X\curData1'; IndividualSlopes1 = b(2,:);
X = [1 1; 1 2; 1 3; 1 4]; b=X\curData2'; IndividualSlopes2 = b(2,:);

figure; 
x = IndividualSlopes1(find(~isnan(IndividualSlopes1(1:53))));
y = IndividualSlopes2(find(~isnan(IndividualSlopes1(1:53))));
scatter(x, y, 'filled'); [r, p] = corrcoef(x,y)

%% association with linear agreement changes

curData1 = agreeModel.HDDM_summary.driftEEG_agree;
curData2 = origModel.HDDM_summary.driftEEG;

X = [1 1; 1 2; 1 3; 1 4; 1 5; 1 6]; b=X\curData1'; IndividualSlopes1 = b(2,:);
X = [1 1; 1 2; 1 3; 1 4]; b=X\curData2'; IndividualSlopes2 = b(2,:);

figure; 
% exclude NaNs within EEG/MRI session
x = IndividualSlopes1(find(~isnan(IndividualSlopes1(1:53))));
y = IndividualSlopes2(find(~isnan(IndividualSlopes1(1:53))));
scatter(x, y, 'filled'); [r, p] = corrcoef(x,y)

curData1 = agreeModel.HDDM_summary.driftMRI_agree;
curData2 = origModel.HDDM_summary.driftMRI;

X = [1 1; 1 2; 1 3; 1 4; 1 5; 1 6]; b=X\curData1'; IndividualSlopes1 = b(2,:);
X = [1 1; 1 2; 1 3; 1 4]; b=X\curData2'; IndividualSlopes2 = b(2,:);

figure; 
% exclude NaNs within EEG/MRI session
x = IndividualSlopes1(find(~isnan(IndividualSlopes1(1:53))));
y = IndividualSlopes2(find(~isnan(IndividualSlopes1(1:53))));
scatter(x, y, 'filled'); [r, p] = corrcoef(x,y)

curData1 = agreeModel.HDDM_summary.nondecisionEEG_agree;
curData2 = origModel.HDDM_summary.nondecisionEEG;

X = [1 1; 1 2; 1 3; 1 4; 1 5; 1 6]; b=X\curData1'; IndividualSlopes1 = b(2,:);
X = [1 1; 1 2; 1 3; 1 4]; b=X\curData2'; IndividualSlopes2 = b(2,:);

figure; 
% exclude NaNs within EEG/MRI session
x = IndividualSlopes1(find(~isnan(IndividualSlopes1(1:53))));
y = IndividualSlopes2(find(~isnan(IndividualSlopes1(1:53))));
scatter(x, y, 'filled'); [r, p] = corrcoef(x,y)


curData1 = agreeModel.HDDM_summary.nondecisionMRI_agree;
curData2 = origModel.HDDM_summary.nondecisionMRI;

X = [1 1; 1 2; 1 3; 1 4; 1 5; 1 6]; b=X\curData1'; IndividualSlopes1 = b(2,:);
X = [1 1; 1 2; 1 3; 1 4]; b=X\curData2'; IndividualSlopes2 = b(2,:);

figure; 
% exclude NaNs within EEG/MRI session
x = IndividualSlopes1(find(~isnan(IndividualSlopes1(1:53))));
y = IndividualSlopes2(find(~isnan(IndividualSlopes1(1:53))));
scatter(x, y, 'filled'); [r, p] = corrcoef(x,y)

