% Calculate reliability of HDDM estimates

% load data as table, convert to array
pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/';
addpath(genpath([pn.root, 'T_tools/']));
dataFile = [pn.root, 'B_data/EEGAgree/m_agree_dim_v_t.csv'];
tableData = readtable(dataFile, 'ReadRowNames', 1);
arrayData = table2array(tableData);
ColumnNames = tableData.Properties.VariableNames'; clear tableData;

% get individual subject data

params = {'v'; 'a'; 't'};
ageGroups = {'1'};
targets = {'1'; '2'; '3'; '4'};

Indices = []; EntryNum = []; MeanValuesEEG = [];
MeanValuesEEG = NaN(numel(params), numel(targets), 102);
for indParam = 1:numel(params)
    for indAge = 1:numel(ageGroups)
        for indCond = 1:numel(targets)
                if strcmp(params{indParam}, 'a')
                    strPattern=[params{indParam}, '_subj_'];
                else
                    strPattern=[params{indParam}, '_subj_',targets{indCond},'_'];
                end
                Indices = find(contains(ColumnNames,strPattern));
                EntryNum{indAge,indParam,indCond} = ...
                    strrep(strrep(cellfun(@(x) x(end-2:end),ColumnNames(Indices),...
                    'un',false), '_', ''),'j', '');
                EntryNum{indAge,indParam,indCond} = cell2mat(cellfun(@str2num,EntryNum{indAge,indParam,indCond},'un',0));
                MeanValuesEEG(indParam,indCond,EntryNum{indAge,indParam,indCond}) = nanmean(arrayData(:,Indices),1);
        end
    end
end
clear EntryNum Indices

% subjects are indexed with respect to IDs_all
pn.dataIn = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/';
load([pn.dataIn, 'SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat'],'IDs_all');

idx_YA = cellfun(@str2num, IDs_all)<2000;
idx_OA = cellfun(@str2num, IDs_all)>2000;

%% plot mean effects using RainCloudPlots
 
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/B_reliabilityAcrossSessions/D_tools/'))

condPairs = [1,2; 2,3; 3,4];
condPairsLevel = [];
condPairsLevel{1} = [5,2.5, 2.2];  
condPairsLevel{2} = [1.6, 1.75, 2];  
condPairsLevel{3} = [.45, .5, .55]; 
lims{1} = [-.5 5.5]; lims{2} = [-2 6]; lims{3} = [0 .6];
%colorm = [43/255 140/255 190/255; 230/255 75/255 50/255; 230/255 75/255 50/255];
colorm = [0.0314, 0.3176, 0.6118; .5,.5,.5; 2.*[.3 .1 .1]]
paramLabels = {'Drift rate'; 'Threshold'; 'Non-decision time'}; 

h = figure('units','normalized','position',[.1 .1 .15 .6]);
for indIdx = 1:3
    subplot(3,1,indIdx)
    cla; hold on;

    curData = squeeze(permute(MeanValuesEEG(indIdx,:,idx_YA),[3,1,2]));
    
 % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = squeeze(curData(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = curData(:,i)-...
                nanmean(curData(:,:),2)+...
                repmat(nanmean(nanmean(curData(:,:),2),1),size(curData(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = colorm(indIdx,:);

    box off
    cla;
        h_rc = rm_raincloud(data_ws, cl,1);
        % add stats
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
               mysigstar_vert(gca, [condPairsLevel{indIdx}(indPair), condPairsLevel{indIdx}(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

            end
        end
        view([90 -90]);
        axis ij
    box(gca,'off')
    %set(gca, 'YTick', [1,2,3,4]);
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    ylabel('Target load'); xlabel(paramLabels{indIdx})
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    xlim(lims{indIdx}); 
    curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);
end

set(findall(gcf,'-property','FontSize'),'FontSize',22)
pn.plotFolder = [pn.root, 'C_figures/'];
figureName = 'C_meanEffects_EEG_YA_vt_RCP';


%% NOW FOR CONVERGENVCE

% load data as table, convert to array
pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/';
addpath(genpath([pn.root, 'T_tools/']));
dataFile = [pn.root, 'B_data/EEGAgree/m_L4_agree_v_t.csv'];
tableData = readtable(dataFile, 'ReadRowNames', 1);
arrayData = table2array(tableData);
ColumnNames = tableData.Properties.VariableNames'; clear tableData;

% get individual subject data

params = {'v'; 'a'; 't'};
ageGroups = {'1'};
targets = {'1'; '3'; '5'; '6'};

Indices = []; EntryNum = []; MeanValuesEEG = [];
MeanValuesEEG = NaN(numel(params), numel(targets), 102);
for indParam = 1:numel(params)
    for indAge = 1:numel(ageGroups)
        for indCond = 1:numel(targets)
                if strcmp(params{indParam}, 'a')
                    strPattern=[params{indParam}, '_subj_'];
                else
                    strPattern=[params{indParam}, '_subj_',targets{indCond},'_'];
                end
                Indices = find(contains(ColumnNames,strPattern));
                EntryNum{indAge,indParam,indCond} = ...
                    strrep(strrep(cellfun(@(x) x(end-2:end),ColumnNames(Indices),...
                    'un',false), '_', ''),'j', '');
                EntryNum{indAge,indParam,indCond} = cell2mat(cellfun(@str2num,EntryNum{indAge,indParam,indCond},'un',0));
                MeanValuesEEG(indParam,indCond,EntryNum{indAge,indParam,indCond}) = nanmean(arrayData(:,Indices),1);
        end
    end
end
clear EntryNum Indices

% subjects are indexed with respect to IDs_all
pn.dataIn = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/';
load([pn.dataIn, 'SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat'],'IDs_all');

idx_YA = cellfun(@str2num, IDs_all)<2000;
idx_OA = cellfun(@str2num, IDs_all)>2000;

%% plot mean effects using RainCloudPlots
 
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/B_reliabilityAcrossSessions/D_tools/'))

condPairs = [1,2; 2,3; 3,4];
condPairsLevel = [];
condPairsLevel{1} = [5,2.5, 2.2];  
condPairsLevel{2} = [1.6, 1.75, 2];  
condPairsLevel{3} = [.45, .5, .55]; 
lims{1} = [-.5 5.5]; lims{2} = [-2 6]; lims{3} = [0 .6];
%colorm = [43/255 140/255 190/255; 230/255 75/255 50/255; 230/255 75/255 50/255];
colorm = [0.0314, 0.3176, 0.6118; .5,.5,.5; 2.*[.3 .1 .1]]
paramLabels = {'Drift rate'; 'Threshold'; 'Non-decision time'}; 

h = figure('units','normalized','position',[.1 .1 .15 .6]);
for indIdx = 1:3
    subplot(3,1,indIdx)
    cla; hold on;

    curData = fliplr(squeeze(permute(MeanValuesEEG(indIdx,:,idx_YA),[3,1,2])));
    
 % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = squeeze(curData(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = curData(:,i)-...
                nanmean(curData(:,:),2)+...
                repmat(nanmean(nanmean(curData(:,:),2),1),size(curData(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = colorm(indIdx,:);

    box off
    cla;
        h_rc = rm_raincloud(data_ws, cl,1);
        % add stats
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
               mysigstar_vert(gca, [condPairsLevel{indIdx}(indPair), condPairsLevel{indIdx}(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

            end
        end
        view([90 -90]);
        axis ij
    box(gca,'off')
    %set(gca, 'YTick', [1,2,3,4]);
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    ylabel('Target load'); xlabel(paramLabels{indIdx})
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    xlim(lims{indIdx}); 
    curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);
end

set(findall(gcf,'-property','FontSize'),'FontSize',22)
pn.plotFolder = [pn.root, 'C_figures/'];
figureName = 'C_meanEffects_EEG_YA_vt_RCP';
