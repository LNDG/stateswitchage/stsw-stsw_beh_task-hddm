% Calculate reliability of HDDM estimates

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data = fullfile(pn.root, 'data');
pn.plotFolder = fullfile(pn.root, 'figures');
addpath(genpath(fullfile(pn.root, 'tools', 'RainCloudPlots')));
addpath(fullfile(pn.root, 'tools'));

% load data as table, convert to array
dataFile = fullfile(pn.data, 'hddm', 'YA_EEG_v_a_t', 'data.csv');
tableData = readtable(dataFile, 'ReadRowNames', 1);
arrayData = table2array(tableData);
ColumnNames = tableData.Properties.VariableNames'; clear tableData;
% get individual subject data

params = {'v'; 'a'; 't'};
ageGroups = {'1'};
conditions = {'1'; '2'; '3'; '4'};

Indices = []; EntryNum = []; MeanValuesEEG = [];
MeanValuesEEG = NaN(numel(params), numel(conditions), 102);
for indParam = 1:numel(params)
    for indAge = 1:numel(ageGroups)
        for indCond = 1:numel(conditions)
            strPattern=[params{indParam}, '_subj_',conditions{indCond},'_'];
            Indices = find(contains(ColumnNames,strPattern));
            EntryNum{indAge,indParam,indCond} = ...
                strrep(cellfun(@(x) x(end-2:end),ColumnNames(Indices),...
                'un',false), '_', '');
            EntryNum{indAge,indParam,indCond} = cell2mat(cellfun(@str2num,EntryNum{indAge,indParam,indCond},'un',0));
            MeanValuesEEG(indParam,indCond,EntryNum{indAge,indParam,indCond}) = nanmean(arrayData(:,Indices),1);
        end
    end
end
clear EntryNum Indices

% subjects are indexed with respect to IDs_all
pn.dataIn = fullfile(pn.root, '..', 'behavior', 'data');
load(fullfile(pn.dataIn, 'SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat'),'IDs_all');

idx_YA = cellfun(@str2num, IDs_all)<2000;
idx_OA = cellfun(@str2num, IDs_all)>2000;

%% plot using RCPs

condPairs = [1,2; 2,3; 3,4];
condPairsLevel = [];
condPairsLevel{1} = [5,2.5, 2.2];  
condPairsLevel{2} = [1.8, 1.9, 2];  
condPairsLevel{3} = [.45, .5, .55]; 
lims{1} = [-.5 5.5]; lims{2} = [0.5 2]; lims{3} = [0 .6];
colorm = [0.0314, 0.3176, 0.6118; 2.*[.3 .1 .1]; 2.*[.3 .1 .1]];
paramLabels = {'Drift rate'; 'Threshold'; 'Non-decision time'}; 

h = figure('units','normalized','position',[.1 .1 .15 .6]);
for indIdx = 1:3
    subplot(3,1,indIdx)
    cla; hold on;

    curData = squeeze(permute(MeanValuesEEG(indIdx,:,idx_YA),[3,1,2]));
    
 % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = squeeze(curData(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = curData(:,i)-...
                nanmean(curData(:,:),2)+...
                repmat(nanmean(nanmean(curData(:,:),2),1),size(curData(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = colorm(indIdx,:);

    box off
    cla;
        h_rc = rm_raincloud(data_ws, cl,1);
        % add stats
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
               mysigstar_vert(gca, [condPairsLevel{indIdx}(indPair), condPairsLevel{indIdx}(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

            end
        end
        view([90 -90]);
        axis ij
    box(gca,'off')
    %set(gca, 'YTick', [1,2,3,4]);
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    ylabel('Target load'); xlabel(paramLabels{indIdx})
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    xlim(lims{indIdx}); 
    curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);
end

set(findall(gcf,'-property','FontSize'),'FontSize',22)
figureName = 'C_meanEffects_EEG_YA_vat_RCP';
saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% SourceData

SourceData_Thresh_EEG = squeeze(MeanValuesEEG(2,:,idx_YA));
SourceData_Thresh_EEG(:,isnan(SourceData_Thresh_EEG(1,:))) = [];
SourceData_Thresh_EEG = SourceData_Thresh_EEG';

%% get MRI-based DDM values to investigate reliability

% load data as table, convert to array
dataFile = fullfile(pn.data, 'hddm', 'YA_MRI_v_a_t', 'data.csv');
tableData = readtable(dataFile, 'ReadRowNames', 1);
arrayData = table2array(tableData);
ColumnNames = tableData.Properties.VariableNames'; clear tableData;

% get individual subject data

params = {'v'; 'a'; 't'};
ageGroups = {'1'};
conditions = {'1'; '2'; '3'; '4'};

Indices = []; EntryNum = []; MeanValuesMRI = [];
MeanValuesMRI = NaN(numel(params), numel(conditions), 102);
for indParam = 1:numel(params)
    for indAge = 1:numel(ageGroups)
        for indCond = 1:numel(conditions)
            strPattern=[params{indParam}, '_subj_',conditions{indCond},'_'];
            Indices = find(contains(ColumnNames,strPattern));
            EntryNum{indAge,indParam,indCond} = ...
                strrep(cellfun(@(x) x(end-2:end),ColumnNames(Indices),...
                'un',false), '_', '');
            EntryNum{indAge,indParam,indCond} = cell2mat(cellfun(@str2num,EntryNum{indAge,indParam,indCond},'un',0));
            MeanValuesMRI(indParam,indCond,EntryNum{indAge,indParam,indCond}) = nanmean(arrayData(:,Indices),1);
        end
    end
end
clear EntryNum Indices

%% plot mean effects on HDDM parameters

condPairs = [1,2; 2,3; 3,4];
condPairsLevel = [];
condPairsLevel{1} = [5,2.5, 2.2];  
condPairsLevel{2} = [1.8, 1.9, 2];  
condPairsLevel{3} = [.45, .5, .55]; 
lims{1} = [-.5 5.5]; lims{2} = [0.5 2]; lims{3} = [0 .6];
colorm = [0.0314, 0.3176, 0.6118; 2.*[.3 .1 .1]; 2.*[.3 .1 .1]];
paramLabels = {'Drift rate'; 'Threshold'; 'Non-decision time'}; 

h = figure('units','normalized','position',[.1 .1 .15 .6]);
for indIdx = 1:3
    subplot(3,1,indIdx)
    cla; hold on;

    curData = squeeze(permute(MeanValuesMRI(indIdx,:,idx_YA),[3,1,2]));
    
 % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = squeeze(curData(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = curData(:,i)-...
                nanmean(curData(:,:),2)+...
                repmat(nanmean(nanmean(curData(:,:),2),1),size(curData(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = colorm(indIdx,:);

    box off
    cla;
        h_rc = rm_raincloud(data_ws, cl,1);
        % add stats
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
               mysigstar_vert(gca, [condPairsLevel{indIdx}(indPair), condPairsLevel{indIdx}(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

            end
        end
        view([90 -90]);
        axis ij
    box(gca,'off')
    %set(gca, 'YTick', [1,2,3,4]);
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    ylabel('Target load'); xlabel(paramLabels{indIdx})
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    xlim(lims{indIdx}); 
    curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);
end

set(findall(gcf,'-property','FontSize'),'FontSize',22)
figureName = 'C_meanEffects_MRI_YA_vat_RCP';
saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% collect DDM estimates in a structure

HDDM_summary.thresholdMRI = squeeze(permute(MeanValuesMRI(2,:,:),[3,2,1]));
HDDM_summary.nondecisionMRI = squeeze(permute(MeanValuesMRI(3,:,:),[3,2,1]));
HDDM_summary.driftMRI = squeeze(permute(MeanValuesMRI(1,:,:),[3,2,1]));

HDDM_summary.thresholdEEG = squeeze(permute(MeanValuesEEG(2,:,:),[3,2,1]));
HDDM_summary.nondecisionEEG = squeeze(permute(MeanValuesEEG(3,:,:),[3,2,1]));
HDDM_summary.driftEEG = squeeze(permute(MeanValuesEEG(1,:,:),[3,2,1]));

HDDM_summary.IDs = IDs_all;

save(fullfile(pn.data, 'HDDM_summary_YA_vat.mat'), 'HDDM_summary')
