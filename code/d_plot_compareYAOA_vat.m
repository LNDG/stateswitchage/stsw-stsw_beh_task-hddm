%% plot comparison of uncertainty effects in HDDM parameters

% load overview structure

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data = fullfile(pn.root, 'data');
pn.plotFolder = fullfile(pn.root, 'figures');
addpath(fullfile(pn.root, 'tools', 'cell2csv'));
addpath(fullfile(pn.root, 'tools'));

load(fullfile(pn.root, '..', '..', 'stsw_multimodal', 'data', 'STSWD_summary_YAOA.mat'))

idx_YA = cellfun(@str2num, STSWD_summary.IDs)<2000;
idx_OA = cellfun(@str2num, STSWD_summary.IDs)>2000;

%% compare l1 HDDM parameter effects

HDDM = [];

tmp = squeeze(nanmean(cat(2,STSWD_summary.HDDM_vat.driftEEG(:,1), STSWD_summary.HDDM_vat.driftMRI(:,1)),2));
tmp(tmp==0)=NaN;
HDDM{1,1} = tmp(idx_YA);
HDDM{2,1} = tmp(idx_OA);

tmp = squeeze(nanmean(cat(2,STSWD_summary.HDDM_vat.thresholdEEG(:,1), STSWD_summary.HDDM_vat.thresholdMRI(:,1)),2));
tmp(tmp==0)=NaN;
HDDM{1,2} = tmp(idx_YA);
HDDM{2,2} = tmp(idx_OA);

tmp = squeeze(nanmean(cat(2,STSWD_summary.HDDM_vat.nondecisionEEG(:,1), STSWD_summary.HDDM_vat.nondecisionMRI(:,1)),2));
tmp(tmp==0)=NaN;
HDDM{1,3} = tmp(idx_YA);
HDDM{2,3} = tmp(idx_OA);

% plot linear estimates using bar charts with individual values

colorm = [.6 .9 1; 1 .6 .6; 1 .6 .6];

parameters = {'Drift', 'Threshold', 'NDT'};
h = figure('units','normalized','position',[.1 .1 .4 .2]);
for indParam = 1:3
    subplot(1,3,indParam)
    plot_data{1} = HDDM{1,indParam};
    plot_data{2} = HDDM{2,indParam};
    set(gcf,'renderer','Painters')
    cla;
    hold on;
    for indGroup = 1:2
        bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(indParam,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        % plot individual values on top
        scatter(repmat(indGroup,numel(plot_data{indGroup}),1)+(rand(numel(plot_data{indGroup}),1)-.5).*.4,...
            plot_data{indGroup}, 20, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    end
    xlim([.25 2.75]); %ylim([1.3 2])
    set(gca,'xtick',[1,2]); set(gca,'xTickLabel',{'YA'; 'OA'}); xlabel('Age Group'); ylabel([parameters{indParam},' L1'])
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    [htest, p] = ttest2(plot_data{1}, plot_data{2});
    title(['p = ', num2str(p)])
end

set(findall(gcf,'-property','FontSize'),'FontSize',22)
figureName = 'D_compareYAOA_vat_l1';
saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');


%% compare linear HDDM parameter effects

HDDM = [];

tmp = squeeze(nanmean(cat(2,STSWD_summary.HDDM_vat.driftEEG_linear, STSWD_summary.HDDM_vat.driftMRI_linear),2));
tmp(tmp==0)=NaN;
HDDM{1,1} = tmp(idx_YA);
HDDM{2,1} = tmp(idx_OA);

tmp = squeeze(nanmean(cat(2,STSWD_summary.HDDM_vat.thresholdEEG_linear, STSWD_summary.HDDM_vat.thresholdMRI_linear),2));
tmp(tmp==0)=NaN;
HDDM{1,2} = tmp(idx_YA);
HDDM{2,2} = tmp(idx_OA);

tmp = squeeze(nanmean(cat(2,STSWD_summary.HDDM_vat.nondecisionEEG_linear, STSWD_summary.HDDM_vat.nondecisionMRI_linear),2));
tmp(tmp==0)=NaN;
HDDM{1,3} = tmp(idx_YA);
HDDM{2,3} = tmp(idx_OA);

% plot linear estimates using bar charts with individual values

colorm = [.6 .9 1; 1 .6 .6; 1 .6 .6];

parameters = {'Drift', 'Threshold', 'NDT'};
h = figure('units','normalized','position',[.1 .1 .4 .2]);
for indParam = 1:3
    subplot(1,3,indParam)
    plot_data{1} = HDDM{1,indParam};
    plot_data{2} = HDDM{2,indParam};
    set(gcf,'renderer','Painters')
    cla;
    hold on;
    for indGroup = 1:2
        bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(indParam,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        % plot individual values on top
        scatter(repmat(indGroup,numel(plot_data{indGroup}),1)+(rand(numel(plot_data{indGroup}),1)-.5).*.4,...
            plot_data{indGroup}, 20, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    end
    xlim([.25 2.75]); %ylim([1.3 2])
    set(gca,'xtick',[1,2]); set(gca,'xTickLabel',{'YA'; 'OA'}); xlabel('Age Group'); ylabel([parameters{indParam},' linear'])
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    [htest, p] = ttest2(plot_data{1}, plot_data{2});
    title(['p = ', num2str(p)])
end

set(findall(gcf,'-property','FontSize'),'FontSize',22)
figureName = 'D_compareYAOA_vat_linearEffects';
saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');


%% prepare output for LMMs in R

% select subjects

 IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
        '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
        '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
        '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
        '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
        '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
        '2252';'2258';'2261'};

% reorganize for R input (long format)

    % select full EEG sample
    idx_Summary = find(ismember(STSWD_summary.IDs, IDs));
    [STSWD_summary.IDs(idx_Summary),IDs]

    dataS = [];
    dataS.Subject = repmat(IDs,1,4); dataS.Subject = repmat(dataS.Subject,2,1); dataS.Subject = reshape(dataS.Subject', [],1);
    dataS.Condition = repmat([1:4],1,numel(IDs)*2); dataS.Condition = reshape(dataS.Condition', [],1);
    dataS.Modality = [repmat({'EEG'}, numel(IDs)*4,1); repmat({'MRI'}, numel(IDs)*4,1)];
    dataS.drift = [reshape(STSWD_summary.HDDM_vat.driftEEG(idx_Summary,1:4)', [],1);...
        reshape(STSWD_summary.HDDM_vat.driftMRI(idx_Summary,1:4)', [],1)];
    dataS.ndt = [reshape(STSWD_summary.HDDM_vat.nondecisionEEG(idx_Summary,1:4)', [],1);...
        reshape(STSWD_summary.HDDM_vat.nondecisionMRI(idx_Summary,1:4)', [],1)];
    dataS.thresh = [reshape(STSWD_summary.HDDM_vat.thresholdEEG(idx_Summary,1:4)', [],1);...
        reshape(STSWD_summary.HDDM_vat.thresholdMRI(idx_Summary,1:4)', [],1)];
    
    dataS.Age = dataS.Subject;
    dataS.Age(cellfun(@str2num, dataS.Subject)< 2000)= {'YA'};
    dataS.Age(cellfun(@str2num, dataS.Subject)>= 2000)= {'OA'};
    
    % combine in data matrix
    data = [];
    data = [dataS.Condition, dataS.drift, dataS.ndt, dataS.thresh];
    data = num2cell(data);
    data = [dataS.Subject,dataS.Age, dataS.Modality, data];

    % add headers
    data = [{'Subject'},{'Age'}, {'Modality'},{'Condition'},{'drift'},{'ndt'},{'thresh'};  data];

    % save for R

    cell2csv(fullfile(pn.data, 'HDDMData_YAOA_forR.csv'),data)
    save(fullfile(pn.data, 'HDDMData_YAOA_forR.mat'),'data')
