% Plot group/wise density of posterior predictive checks (HDDM)

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data = fullfile(pn.root, 'data');
pn.plotFolder = fullfile(pn.root, 'figures');

% load data as table, convert to array
dataFile = fullfile(pn.data, 'hddm', 'YA_EEG_v_a_t', 'ppc.csv');

% encode numpy table her
filetext = fileread(dataFile);
ll = strsplit(filetext,{'\n', '\t'},'CollapseDelimiters',true);
data = reshape(ll,5,[])';
data(1,:) = []; % delete header

% designate labels
sample = cellfun(@str2num,data(:,1));
trial = cellfun(@str2num,data(:,2));
rt = cellfun(@str2num,data(:,3));
acc = cellfun(@str2num,data(:,4));
subcondidx = data(:,5);

clear data ll;

% get individual subject data
% calculate pdf across samples
% avg across subjects, within target load conditions

RTs = [];
for indLoad = 1:4
    for indSub = 1:49
        searchstrg = ['wfpt(', num2str(indLoad), ').', num2str(indSub)];
        idx_1 = strcmp(subcondidx, searchstrg);
        if isempty(find(idx_1))
            warning(['No data for ', searchstrg]);
            continue;
        end
        %hist_tmp = histogram(rt(idx_1), linspace(-2.5,2.5,100),'Normalization', 'pdf');
        % 100 bins across all samples
        RTs(indLoad, indSub,:) = histcounts(rt(idx_1), linspace(-2.5,2.5,100),'Normalization', 'probability');
    end
end

%% compare with empirical data (EEG session)

load(fullfile(pn.data, 'StateSwitchDynamicTrialData_EEG_YA.mat'), 'data_YA');

% recode errors as negative

rt_data = data_YA(:,3);
rt_data(data_YA(:,2)==0) = rt_data(data_YA(:,2)==0)*-1;

RTs_data = [];
for indLoad = 1:4
    RTs_data(indLoad, 1,:) = histcounts(rt_data(data_YA(:,4) == indLoad), linspace(-2.5,2.5,100),'Normalization', 'probability');
end

h = figure('units','normalized','position',[.1 .1 .2 .4]);
set(gcf,'renderer','Painters')
boundaries = linspace(-2.5,2.5,100);
subplot(3,2,1);
hold on; plot(boundaries(2:end),squeeze(nanmean(RTs(1,:,:),2)), 'LineWidth', 2);
plot(boundaries(2:end),squeeze(nanmean(RTs(2,:,:),2)), 'LineWidth', 2);
plot(boundaries(2:end),squeeze(nanmean(RTs(3,:,:),2)), 'LineWidth', 2);
plot(boundaries(2:end),squeeze(nanmean(RTs(4,:,:),2)), 'LineWidth', 2);
ylabel({'Probability';'density'}); xlabel('RT bin'); xlim([-2.5, 2.5]);%ylim([0 .25])
title('Model-based RT')
subplot(3,2,2);
hold on; plot(boundaries(2:end),squeeze(nanmean(RTs_data(1,:,:),2)), 'LineWidth', 2);
plot(boundaries(2:end),squeeze(nanmean(RTs_data(2,:,:),2)), 'LineWidth', 2);
plot(boundaries(2:end),squeeze(nanmean(RTs_data(3,:,:),2)), 'LineWidth', 2);
plot(boundaries(2:end),squeeze(nanmean(RTs_data(4,:,:),2)), 'LineWidth', 2);
ylabel('Prob. density'); xlabel('RT bin'); xlim([-2.5, 2.5]); %ylim([0 .25])
title('Empirical RT')
for indPlot = 1:4
   subplot(3,2,indPlot+2);
   hold on; histogram(rt_data(data_YA(:,4) == indPlot),linspace(-2.5,2.5,100),'Normalization', 'probability');
   hold on; plot(boundaries(2:end),squeeze(nanmean(RTs(indPlot,:,:),2)), 'LineWidth', 2);
   ylabel('Prob. density'); xlabel('Reaction time (s)'); xlim([-2.5, 2.5]);%ylim([0 .25])
   title([num2str(indPlot), ' Targets']);
end
set(findall(gcf,'-property','FontSize'),'FontSize',16)

figureName = 'c0_PPC_YA_vat';

saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');