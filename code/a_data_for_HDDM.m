% bring data into HDDM format:
% Columns: condition (seperate subjects here), correct/incorrect, RTs (in s) 

clear all; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.dataIn = fullfile(pn.root, '..', 'behavior','data');
pn.dataOut = fullfile(pn.root, 'data');

addpath(fullfile(pn.root, 'tools', 'cell2csv'));

load(fullfile(pn.dataIn, 'SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat'),...
    'MergedDataEEG', 'MergedDataMRI', 'IDs_all');

modalities = {'EEG'; 'MRI'};

for indModality = 1:numel(modalities)

    %% get vectors of RTs and Acc

    RTs_vector = reshape(eval(['MergedData', modalities{indModality}, '.RTs']),1,[]);
    Acc_vector = reshape(eval(['MergedData', modalities{indModality}, '.Accs']),1,[]);

    %% create vector of IDs

    Sub_vector = reshape(repmat(1:102,256,1),1,[]);

    %% create vector of age

    ageVec = [];
    IDs = cell2mat(IDs_all);
    IDs = str2mat(IDs(:,1));
    for indEntry = 1:numel(IDs)
        if strcmp(IDs(indEntry), '1')
            %ageVec{1,indEntry} = 'YA';
            ageVec(1,indEntry) = 1;
        elseif strcmp(IDs(indEntry), '2')
            %ageVec{1,indEntry} = 'OA';
            ageVec(1,indEntry) = 2;
        end
    end; clear IDs;

    Age_vector = reshape(repmat(ageVec,256,1),1,[]);

    %% create vector of dimensionality

    Dim_vector = reshape(eval(['MergedData', modalities{indModality}, '.StateOrders']),1,[]);

    %% create vector of attribute

    Att_vector = reshape(eval(['MergedData', modalities{indModality}, '.Atts']),1,[]);

    %% combine in data matrix

    data = [];
    data = [Sub_vector', Acc_vector', RTs_vector', Dim_vector', Att_vector', Age_vector'];

    %% set any RTs below 250 ms to NaN

    data(find(RTs_vector<=.25),:) = NaN;

    %figure; plot(sort(RTs_vector,'ascend')); ylim([0 .25])

    %% delete any rows with nans

    data(any(isnan(data), 2), :) = [];

    %% add complete ID

    IDs_mat =  cellfun(@str2num,IDs_all);
    data(:,7) = IDs_mat(data(:,1)); clear IDs_mat;

    %% export data matrix

    data_YA = [data(data(:,7)<2000,:)];
    data_OA = [data(data(:,7)>=2000,:)];
    save(fullfile(pn.dataOut, ['StateSwitchDynamicTrialData_',modalities{indModality},'.mat']),'data')
    save(fullfile(pn.dataOut, ['StateSwitchDynamicTrialData_',modalities{indModality},'_YA.mat']),'data_YA')
    save(fullfile(pn.dataOut, ['StateSwitchDynamicTrialData_',modalities{indModality},'_OA.mat']),'data_OA')

    %% add headers

    data = num2cell(data);
    data = [{'subject'},{'acc'},{'rt'},{'dim'},{'att'},{'age'},{'ID'};data];

    %% export data matrix as .csv

    cell2csv(fullfile(pn.dataOut, ['StateSwitchDynamicTrialData_',modalities{indModality},'.dat']),data)

    %% create separate structures for younger and older adults

    fields = cellfun(@(x)(x>=2000), data(:,7), 'UniformOutput', 0);
    OAinds = cell2mat(fields(2:end));

    data_YA = [data(1,:); data(find(OAinds==0)+1,:)];
    data_OA = [data(1,:); data(find(OAinds==1)+1,:)];

    cell2csv(fullfile(pn.dataOut, ['StateSwitchDynamicTrialData_',modalities{indModality},'_YA.dat']),data_YA)
    cell2csv(fullfile(pn.dataOut, ['StateSwitchDynamicTrialData_',modalities{indModality},'_OA.dat']),data_OA)
end