% plot DICs of different models

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.plotFolder = fullfile(pn.root, 'figures');

% manually extracted DIC values from HDDM output (EEG session)
DIC{1}.EEG.null = 13523;
DIC{1}.EEG.v = 10595;
DIC{1}.EEG.va = 8282;
DIC{1}.EEG.vt = 6798;
DIC{1}.EEG.vat = 6107;

% manually extracted DIC values from HDDM output (MRI session)
DIC{1}.MRI.null = 9515;
DIC{1}.MRI.v = 7668;
DIC{1}.MRI.va = 5552;
DIC{1}.MRI.vt = 3831;
DIC{1}.MRI.vat = 3357;

% manually extracted DIC values from HDDM output (EEG session)
DIC{2}.EEG.null = 21782;
DIC{2}.EEG.v = 20706;
DIC{2}.EEG.va = 18596;
DIC{2}.EEG.vt = 17820;
DIC{2}.EEG.vat = 17218;

% manually extracted DIC values from HDDM output (MRI session)
DIC{2}.MRI.null = 18752;
DIC{2}.MRI.v = 17852;
DIC{2}.MRI.va = 15244;
DIC{2}.MRI.vt = 14388;
DIC{2}.MRI.vat = 13627;

h = figure('units','centimeters','position',[0 0 10 8]);
b = bar([DIC{1}.EEG.v-DIC{1}.EEG.null, DIC{1}.MRI.v-DIC{1}.MRI.null,...
    DIC{2}.EEG.v-DIC{2}.EEG.null, DIC{2}.MRI.v-DIC{2}.MRI.null;...
    DIC{1}.EEG.va-DIC{1}.EEG.null, DIC{1}.MRI.va-DIC{1}.MRI.null,...
    DIC{2}.EEG.va-DIC{2}.EEG.null, DIC{2}.MRI.va-DIC{2}.MRI.null;...
    DIC{1}.EEG.vt-DIC{1}.EEG.null, DIC{1}.MRI.vt-DIC{1}.MRI.null,...
    DIC{2}.EEG.vt-DIC{2}.EEG.null, DIC{2}.MRI.vt-DIC{2}.MRI.null;...
    DIC{1}.EEG.vat-DIC{1}.EEG.null, DIC{1}.MRI.vat-DIC{1}.MRI.null,...
    DIC{2}.EEG.vat-DIC{2}.EEG.null, DIC{2}.MRI.vat-DIC{2}.MRI.null]);
ylabel({'DIC difference','from null model'})
leg = legend([b(1), b(2),b(3), b(4)],{'YA: EEG'; 'YA: MRI'; 'OA: EEG'; 'OA: MRI'}, ...
    'orientation', 'vertical', 'location', 'SouthWest'); legend('boxoff')
set(b(1), 'FaceColor',[0 0 0])
set(b(2), 'FaceColor',[.5 .5 .5], 'EdgeColor', [.5 .5 .5]);
grid minor; 
curTicks = get(gca, 'ytick'); set(gca, 'ytick', curTicks(2:2:end));
set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'+v', '+va', ...
    '+vt', '+vat'});
set(findall(gcf,'-property','FontSize'),'FontSize',18);
set(leg,'FontSize',7);

figureName = 'c0_DICcomparison';

saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');
