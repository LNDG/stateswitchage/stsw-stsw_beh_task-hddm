%% plot comparison of uncertainty effects in HDDM parameters

% load overview structure

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;
pn.plotFolder = fullfile(pn.root, 'figures');

load(fullfile(pn.root, '..', '..', 'stsw_multimodal', 'data', 'STSWD_summary_YAOA.mat'))

% 2139, 2227 rm due to incidental findings
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

idx_YA = IDs(cellfun(@str2num, IDs)<2000); idx_YA = ismember(STSWD_summary.IDs, idx_YA);
idx_OA = IDs(cellfun(@str2num, IDs)>2000); idx_OA = ismember(STSWD_summary.IDs, idx_OA);

HDDM = [];

tmp = squeeze(STSWD_summary.CPPslopes.data(:,1));
tmp(tmp==0)=NaN;
HDDM{1,1} = tmp(idx_YA);
HDDM{2,1} = tmp(idx_OA);

tmp = squeeze(STSWD_summary.CPPslopes.linear_win(:,1));
tmp(tmp==0)=NaN;
HDDM{1,2} = tmp(idx_YA);
HDDM{2,2} = tmp(idx_OA);

HDDM{1,3} = HDDM{1,2};
HDDM{2,3} = HDDM{2,2};

% plot linear estimates using bar charts with individual values

colorm = [.6 .9 1; .6 .9 1; .6 .9 1];

parameters = {'CPP L1', 'CPP linear_win','CPP linear_win'};
h = figure('units','normalized','position',[.1 .1 .2 .2]);
for indParam = 1:3
    subplot(1,3,indParam)
    plot_data{1} = HDDM{1,indParam};
    plot_data{2} = HDDM{2,indParam};
    set(gcf,'renderer','Painters')
    cla;
    hold on;
    for indGroup = 1:2
        bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(indParam,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        % plot individual values on top
        scatter(repmat(indGroup,numel(plot_data{indGroup}),1)+(rand(numel(plot_data{indGroup}),1)-.5).*.4,...
            plot_data{indGroup}, 20, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    end
    xlim([.25 2.75]); %ylim(ylims(indParam,:))
    set(gca,'xtick',[1,2]); set(gca,'xTickLabel',{'YA'; 'OA'}); xlabel('Age Group'); ylabel([parameters{indParam}])
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    [h, p] = ttest2(plot_data{1}, plot_data{2});
    title(['p = ', num2str(p)])
end

set(findall(gcf,'-property','FontSize'),'FontSize',22)
figureName = 'D_compareYAOA_CPP_L1_linear';
saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% correlation drift rate -- CPP linear_win (& modulation)

% YA
x = STSWD_summary.CPPslopes.data(idx_YA,1);
y = squeeze(nanmean(cat(2,STSWD_summary.HDDM_vt.driftEEG(idx_YA,1), STSWD_summary.HDDM_vt.driftMRI(idx_YA,1)),2));
figure; scatter(x,y,'filled')
[r,p] = corrcoef(x,y)

% OA
x = STSWD_summary.CPPslopes.data(idx_OA,1);
y = squeeze(nanmean(cat(2,STSWD_summary.HDDM_vt.driftEEG(idx_OA,1), STSWD_summary.HDDM_vt.driftMRI(idx_OA,1)),2));
figure; scatter(x,y,'filled')
[r,p] = corrcoef(x,y)

% all
x = STSWD_summary.CPPslopes.data(:,1);
y = squeeze(nanmean(cat(2,STSWD_summary.HDDM_vt.driftEEG(:,1), STSWD_summary.HDDM_vt.driftMRI(:,1)),2));
figure; scatter(x,y,'filled')
[r,p] = corrcoef(x,y)
% controlling for age
X = [x,y,cellfun(@str2num, STSWD_summary.IDs)>2000];
[r,p] = partialcorr(X)

%% modulation

% YA
x = STSWD_summary.CPPslopes.linear_win(idx_YA,1);
y = squeeze(nanmean(cat(2,STSWD_summary.HDDM_vt.driftEEG_linear(idx_YA,1), STSWD_summary.HDDM_vt.driftMRI_linear(idx_YA,1)),2));
figure; scatter(x,y,'filled')
[r,p] = corrcoef(x,y)

% OA
x = STSWD_summary.CPPslopes.linear_win(idx_OA,1);
y = squeeze(nanmean(cat(2,STSWD_summary.HDDM_vt.driftEEG_linear(idx_OA,1), STSWD_summary.HDDM_vt.driftMRI_linear(idx_OA,1)),2));
figure; scatter(x,y,'filled')
[r,p] = corrcoef(x,y)

% all
x = STSWD_summary.CPPslopes.linear_win(:,1);
y = squeeze(nanmean(cat(2,STSWD_summary.HDDM_vt.driftEEG_linear(:,1), STSWD_summary.HDDM_vt.driftMRI_linear(:,1)),2));
figure; scatter(x,y,'filled')
[r,p] = corrcoef(x,y)
% controlling for age
X = [x,y,cellfun(@str2num, STSWD_summary.IDs)>2000];
[r,p] = partialcorr(X)

