#!/bin/bash

fun_name="b_HDDM_modeling"
job_name="stsw_hddm"

mkdir ./logs

rootpath="$(pwd)/../.."
rootpath=$(builtin cd $rootpath; pwd)

modality="EEG MRI"
group="YA OA"
params="null v va vt vat"

for modality_name in $modality; do 
for group_name in $group; do 
for params_name in $params; do 
  	sbatch \
  		--job-name ${job_name}_${modality_name}_${group_name}_${params_name} \
  		--cpus-per-task 1 \
  		--mem 1gb \
  		--time 05:00:00 \
  		--output ./logs/${job_name}_${group_name}_${modality_name}_${params_name}.out \
  		--workdir . \
  		--wrap="jupyter nbconvert --ExecutePreprocessor.kernel_name='hddm_env' --ExecutePreprocessor.timeout=None --Application.log_level=10 --to notebook --inplace --execute $(pwd)/${fun_name}_${modality_name}_${group_name}_${params_name}.ipynb"
done
done
done