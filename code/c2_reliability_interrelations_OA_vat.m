currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data = fullfile(pn.root, 'data');
pn.plotFolder = fullfile(pn.root, 'figures');
addpath(genpath(fullfile(pn.root, 'tools', 'RainCloudPlots')));
addpath(fullfile(pn.root, 'tools', 'BrewerMap')); cBrew = brewermap(4,'RdBu');
addpath(fullfile(pn.root, 'tools'));

% load merged data matrix
load(fullfile(pn.data, 'HDDM_summary_OA_vat.mat'), 'HDDM_summary')

idx_YA = cellfun(@str2num, HDDM_summary.IDs)<2000;
idx_OA = cellfun(@str2num, HDDM_summary.IDs)>2000;

%% plot reliability

h = figure('units','normalized','position',[.1 .1 .35 .35]);
p = []; r = []; y_ls = [];
subplot(3,3,[1,4]); cla;
    title('Threshold (T.)'); hold on;
    for indCond = 1
        x1 = HDDM_summary.thresholdEEG(idx_OA,indCond);
        y1 = HDDM_summary.thresholdMRI(idx_OA,indCond);
        x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
        y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
        scatter(x, y, 'filled', 'MarkerFaceColor', [0 0 0]);
        y_ls{indCond} = polyval(polyfit(x,y,1),x); y_ls{indCond} = plot(x, y_ls{indCond}, 'Color', [0 0 0], 'LineWidth', 1);
        [r{indCond}, p{indCond}] = corrcoef(x,y, 'rows', 'complete'); p{indCond} = convertPtoExponential(p{indCond}(2));
    end
    xlabel('T. EEG Session'); ylabel('Threshold MRI session');
    lg1 = legend([y_ls{1}], {['r = ', num2str(round(r{1}(2),2)), ', p = ' p{1}{1}]}, 'location', 'South'); legend('boxoff');
    ylim([-0 2]); xlim([.8 2])
subplot(3,3,[2,5]); cla;
    title('Non-decision time (NDT)'); hold on;
    for indCond = 1:4
        x1 = HDDM_summary.nondecisionEEG(idx_OA,indCond);
        y1 = HDDM_summary.nondecisionMRI(idx_OA,indCond);
        x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
        y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
        scatter(x, y, 'filled', 'MarkerFaceColor', cBrew(indCond,:));
        y_ls{indCond} = polyval(polyfit(x,y,1),x); y_ls{indCond} = plot(x, y_ls{indCond}, 'Color', cBrew(indCond,:), 'LineWidth', 1);
        [r{indCond}, p{indCond}] = corrcoef(x,y, 'rows', 'complete'); p{indCond} = convertPtoExponential(p{indCond}(2));
    end
    lg2 = legend([y_ls{1}, y_ls{2}, y_ls{3}, y_ls{4}], {['L1: r = ', num2str(round(r{1}(2),2)), ', p = ' p{1}{1}], ...
            ['L2: r = ', num2str(round(r{2}(2),2)), ', p = ' p{2}{1}],...
            ['L3: r = ', num2str(round(r{3}(2),2)), ', p = ' p{3}{1}],...
            ['L4: r = ', num2str(round(r{4}(2),2)), ', p = ' p{4}{1}]}, 'location', 'South'); legend('boxoff');
    xlabel('NDT EEG Session'); ylabel('NDT MRI session');
    ylim([-0 .6])
subplot(3,3,[3,6]); cla;
    title('Drift rate'); hold on;
    for indCond = 1:4
        x1 = HDDM_summary.driftEEG(idx_OA,indCond);
        y1 = HDDM_summary.driftMRI(idx_OA,indCond);
        x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
        y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
        scatter(x, y, 'filled', 'MarkerFaceColor', cBrew(indCond,:));
        y_ls{indCond} = polyval(polyfit(x,y,1),x); y_ls{indCond} = plot(x, y_ls{indCond}, 'Color', cBrew(indCond,:), 'LineWidth', 1);
        [r{indCond}, p{indCond}] = corrcoef(x,y, 'rows', 'complete'); p{indCond} = convertPtoExponential(p{indCond}(2));
    end
    xlabel('Drift EEG Session'); ylabel('Drift MRI session');
    lg3 = legend([y_ls{1}, y_ls{2}, y_ls{3}, y_ls{4}], {['L1: r = ', num2str(round(r{1}(2),2)), ', p = ' p{1}{1}], ...
            ['L2: r = ', num2str(round(r{2}(2),2)), ', p = ' p{2}{1}],...
            ['L3: r = ', num2str(round(r{3}(2),2)), ', p = ' p{3}{1}],...
            ['L4: r = ', num2str(round(r{4}(2),2)), ', p = ' p{4}{1}]}, 'location', 'South'); legend('boxoff');
    ylim([-1.5 3.5])
    clear p r y_ls
subplot(3,3,7); cla; hold on;
    x1 = HDDM_summary.thresholdEEG(idx_OA,4)-HDDM_summary.thresholdEEG(idx_OA,1);
    y1 = HDDM_summary.thresholdMRI(idx_OA,4)-HDDM_summary.thresholdMRI(idx_OA,1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'T. EEG Session', 'L4-L1 change'}); ylabel({'T. MRI session';'L4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    ylim([-1 1]); xlim([-.3 1.25])
subplot(3,3,8); cla;hold on;
    x1 = HDDM_summary.nondecisionEEG(idx_OA,4)-HDDM_summary.nondecisionEEG(idx_OA,1);
    y1 = HDDM_summary.nondecisionMRI(idx_OA,4)-HDDM_summary.nondecisionMRI(idx_OA,1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'NDT EEG Session', 'L4-L1 change'}); ylabel({'NDT MRI session';'L4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg5 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    ylim([-.4 .4]); xlim([-.1 .4])
subplot(3,3,9); cla; hold on;
    x1 = HDDM_summary.driftEEG(idx_OA,4)-HDDM_summary.driftEEG(idx_OA,1);
    y1 = HDDM_summary.driftMRI(idx_OA,4)-HDDM_summary.driftMRI(idx_OA,1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift EEG Session', 'L4-L1 change'}); ylabel({'Drift MRI session';'L4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg6 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    ylim([-5 1]); xlim([-4 0])
set(findall(gcf,'-property','FontSize'),'FontSize',15)
set(lg1,'FontSize',12)
set(lg2,'FontSize',12)
set(lg3,'FontSize',12)
set(lg5,'FontSize',12)
set(lg6,'FontSize',12)

figureName = 'C_HDDMreliability_vat';

saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% plot parameter inter-correlations (also with RT, Acc)

h = figure('units','normalized','position',[.1 .1 .35 .35]);

% intercept-intercept across parameters

subplot(3,3,1); cla; hold on;
    %title('L4-L1 Threshold change'); hold on;
    x1 = HDDM_summary.driftEEG(idx_OA,1);
    y1 = HDDM_summary.thresholdEEG(idx_OA,1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'L1'}); ylabel({'Threshold';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');

subplot(3,3,2); cla; hold on;
    x1 = HDDM_summary.driftEEG(idx_OA,1);
    y1 = HDDM_summary.nondecisionEEG(idx_OA,1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'L1'}); ylabel({'NDT';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    
subplot(3,3,3); cla; hold on;
    x1 = HDDM_summary.thresholdEEG(idx_OA,1);
    y1 = HDDM_summary.nondecisionEEG(idx_OA,1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Threshold', 'L1'}); ylabel({'NDT';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    
% change-change correlation

subplot(3,3,4); cla; hold on;
    x1 = nanmean(HDDM_summary.driftEEG(idx_OA,2:4),2)-HDDM_summary.driftEEG(idx_OA,1);
    y1 = nanmean(HDDM_summary.nondecisionEEG(idx_OA,2:4),2)-HDDM_summary.nondecisionEEG(idx_OA,1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'L2:4-L1 change'}); ylabel({'NDT';'L2:4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');

%% Intercept-Change correlations

h = figure('units','normalized','position',[.1 .1 .35 .35]);
 
 suptitle('Intercept-Change correlations')

 subplot(3,3,1); cla; hold on;
    x1 = nanmean(HDDM_summary.driftEEG(idx_OA,1),2)
    y1 = nanmean(HDDM_summary.driftEEG(idx_OA,2:4),2)-HDDM_summary.driftEEG(idx_OA,1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'L1'}); ylabel({'Drift';'L2:4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');

 subplot(3,3,2); cla; hold on;
    x1 = nanmean(HDDM_summary.thresholdEEG(idx_OA,1),2)
    y1 = nanmean(HDDM_summary.driftEEG(idx_OA,2:4),2)-HDDM_summary.driftEEG(idx_OA,1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Threshold', 'L1'}); ylabel({'Drift';'L2:4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');

  subplot(3,3,3); cla; hold on;
    x1 = nanmean(HDDM_summary.nondecisionEEG(idx_OA,1),2)
    y1 = nanmean(HDDM_summary.driftEEG(idx_OA,2:4),2)-HDDM_summary.driftEEG(idx_OA,1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'NDT', 'L1'}); ylabel({'Drift';'L2:4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');

 subplot(3,3,4); cla; hold on;
    x1 = nanmean(HDDM_summary.driftEEG(idx_OA,1),2);
    y1 = nanmean(HDDM_summary.thresholdEEG(idx_OA,2:4),2)-nanmean(HDDM_summary.thresholdEEG(idx_OA,1),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'L1'}); ylabel({'Threshold';'L2:4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');

 subplot(3,3,5); cla; hold on;
    x1 = nanmean(HDDM_summary.thresholdEEG(idx_OA,1),2);
    y1 = nanmean(HDDM_summary.thresholdEEG(idx_OA,2:4),2)-nanmean(HDDM_summary.thresholdEEG(idx_OA,1),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Threshold', 'L1'}); ylabel({'Threshold';'L2:4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');

  subplot(3,3,6); cla; hold on;
    x1 = nanmean(HDDM_summary.nondecisionEEG(idx_OA,1),2);
    y1 = nanmean(HDDM_summary.thresholdEEG(idx_OA,2:4),2)-nanmean(HDDM_summary.thresholdEEG(idx_OA,1),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'NDT', 'L1'}); ylabel({'Threshold';'L2:4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');

  subplot(3,3,7); cla; hold on;
    x1 = nanmean(HDDM_summary.driftEEG(idx_OA,1),2)
    y1 = nanmean(HDDM_summary.nondecisionEEG(idx_OA,2:4),2)-HDDM_summary.nondecisionEEG(idx_OA,1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'L1'}); ylabel({'NDT';'L2:4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');

 subplot(3,3,8); cla; hold on;
    x1 = nanmean(HDDM_summary.thresholdEEG(idx_OA,1),2)
    y1 = nanmean(HDDM_summary.nondecisionEEG(idx_OA,2:4),2)-HDDM_summary.nondecisionEEG(idx_OA,1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Threshold', 'L1'}); ylabel({'NDT';'L2:4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');

  subplot(3,3,9); cla; hold on;
    x1 = nanmean(HDDM_summary.nondecisionEEG(idx_OA,1),2)
    y1 = nanmean(HDDM_summary.nondecisionEEG(idx_OA,2:4),2)-HDDM_summary.nondecisionEEG(idx_OA,1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'NDT', 'L1'}); ylabel({'NDT';'L2:4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');

figureName = 'C_Interrelations_interceptChange_vat';

saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');


%% Correlations with Accuracy and RT
% intercept & change-change

load(fullfile(pn.root, '..', '..', 'stsw_multimodal', 'data', 'STSWD_summary_YAOA.mat'))

idxSummaryIDs = find(ismember(STSWD_summary.IDs, HDDM_summary.IDs));

h = figure('units','normalized','position',[.1 .1 .35 .3]);
 
 %suptitle('Intercept-Intercept correlations')

 subplot(2,3,1); cla; hold on;
    x1 = squeeze(nanmean(HDDM_summary.driftEEG(idx_OA,1),2));
    y1 = STSWD_summary.behav.EEGRT(idxSummaryIDs(idx_OA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'L1'}); ylabel({'RT';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'NorthEast'); legend('boxoff');
 subplot(2,3,2); cla; hold on;
    x1 = squeeze(nanmean(HDDM_summary.thresholdEEG(idx_OA,1),2));
    y1 = STSWD_summary.behav.EEGRT(idxSummaryIDs(idx_OA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Threshold', 'L1'}); ylabel({'RT';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'NorthWest'); legend('boxoff');
 subplot(2,3,3); cla; hold on;
    x1 = squeeze(nanmean(HDDM_summary.nondecisionEEG(idx_OA,1),2));
    y1 = STSWD_summary.behav.EEGRT(idxSummaryIDs(idx_OA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'NDT', 'L1'}); ylabel({'RT';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'NorthEast'); legend('boxoff');

 subplot(2,3,4); cla; hold on;
    x1 = squeeze(nanmean(HDDM_summary.driftEEG(idx_OA,1),2));
    y1 = STSWD_summary.behav.EEGAcc(idxSummaryIDs(idx_OA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'L1'}); ylabel({'Accuracy';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
 subplot(2,3,5); cla; hold on;
    x1 = squeeze(nanmean(HDDM_summary.thresholdEEG(idx_OA,1),2));
    y1 = STSWD_summary.behav.EEGAcc(idxSummaryIDs(idx_OA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Threshold', 'L1'}); ylabel({'Accuracy';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
 subplot(2,3,6); cla; hold on;
    x1 = squeeze(nanmean(HDDM_summary.nondecisionEEG(idx_OA,1),2));
    y1 = STSWD_summary.behav.EEGAcc(idxSummaryIDs(idx_OA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'NDT', 'L1'}); ylabel({'Accuracy';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');

    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
figureName = 'C_RTACC_bl_vat';

saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');
