% Calculate reliability of HDDM estimates

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data = fullfile(pn.root, 'data');
pn.plotFolder = fullfile(pn.root, 'figures');
addpath(genpath(fullfile(pn.root, 'tools', 'RainCloudPlots')));
addpath(fullfile(pn.root, 'tools'));

% load data as table, convert to array
dataFile = fullfile(pn.data, 'hddm', 'OA_EEG_v_t', 'data.csv');
tableData = readtable(dataFile, 'ReadRowNames', 1);
arrayData = table2array(tableData);
ColumnNames = tableData.Properties.VariableNames'; clear tableData;

% get individual subject data

params = {'v'; 'a'; 't'};
ageGroups = {'1'};
conditions = {'1'; '2'; '3'; '4'};

Indices = []; EntryNum = []; MeanValuesEEG = [];
MeanValuesEEG = NaN(numel(params), numel(conditions), 102);
for indParam = 1:numel(params)
    for indAge = 1:numel(ageGroups)
        for indCond = 1:numel(conditions)
            if strcmp(params{indParam}, 'a')
                strPattern=[params{indParam}, '_subj_'];
            else
                strPattern=[params{indParam}, '_subj_',conditions{indCond},'_'];
            end
            Indices = find(contains(ColumnNames,strPattern));
            EntryNum{indAge,indParam,indCond} = ...
                strrep(strrep(cellfun(@(x) x(end-2:end),ColumnNames(Indices),...
                'un',false), '_', ''),'j', '');
            EntryNum{indAge,indParam,indCond} = cell2mat(cellfun(@str2num,EntryNum{indAge,indParam,indCond},'un',0));
            MeanValuesEEG(indParam,indCond,EntryNum{indAge,indParam,indCond}) = nanmean(arrayData(:,Indices),1);
        end
    end
end
clear EntryNum Indices

% subjects are indexed with respect to IDs_all
pn.dataIn = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/';
load([pn.dataIn, 'SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat'],'IDs_all');

idx_OA = cellfun(@str2num, IDs_all)<2000;
idx_OA = cellfun(@str2num, IDs_all)>2000;

%% plot mean effects on HDDM parameters

condPairs = [1,2; 2,3; 3,4];
condPairsLevel{1} = [3.1, 1.75, 1.35];  
condPairsLevel{2} = [1.6, 1.75, 2];  
condPairsLevel{3} = [.35, .45, .55];  
colorm = [0.0314, 0.3176, 0.6118; .5,.5,.5; 2.*[.3 .1 .1]];
paramLabels = {'Drift rate'; 'Threshold'; 'Non-decision time'}; 

h = figure('units','normalized','position',[.1 .1 .15 .6]);
for indIdx = 1:3
    subplot(3,1,indIdx)
    cla; hold on;

    curData = squeeze(permute(MeanValuesEEG(indIdx,:,idx_OA),[3,1,2]));
    
 % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = squeeze(curData(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = curData(:,i)-...
                nanmean(curData(:,:),2)+...
                repmat(nanmean(nanmean(curData(:,:),2),1),size(curData(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = colorm(indIdx,:);

    box off
    cla;
        h_rc = rm_raincloud(data_ws, cl,1);
        % add stats
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
               mysigstar_vert(gca, [condPairsLevel{indIdx}(indPair), condPairsLevel{indIdx}(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

            end
        end
        view([90 -90]);
        axis ij
    box(gca,'off')
    %set(gca, 'YTick', [1,2,3,4]);
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    ylabel('Target load'); xlabel(paramLabels{indIdx})
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    xlim(lims{indIdx}); 
    curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);
end

set(findall(gcf,'-property','FontSize'),'FontSize',22)
figureName = 'C_meanEffects_EEG_OA_vt_RCP';
saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% get MRI-based DDM values to investigate reliability

% load data as table, convert to array
dataFile = fullfile(pn.data, 'hddm', 'OA_MRI_v_t', 'data.csv');
tableData = readtable(dataFile, 'ReadRowNames', 1);
arrayData = table2array(tableData);
ColumnNames = tableData.Properties.VariableNames'; clear tableData;

% get individual subject data

params = {'v'; 'a'; 't'};
ageGroups = {'1'};
conditions = {'1'; '2'; '3'; '4'};

Indices = []; EntryNum = []; MeanValuesMRI = [];
MeanValuesMRI = NaN(numel(params), numel(conditions), 102);
for indParam = 1:numel(params)
    for indAge = 1:numel(ageGroups)
        for indCond = 1:numel(conditions)
            if strcmp(params{indParam}, 'a')
                strPattern=[params{indParam}, '_subj_'];
            else
                strPattern=[params{indParam}, '_subj_',conditions{indCond},'_'];
            end
            Indices = find(contains(ColumnNames,strPattern));
            EntryNum{indAge,indParam,indCond} = ...
                strrep(strrep(cellfun(@(x) x(end-2:end),ColumnNames(Indices),...
                'un',false), '_', ''),'j', '');
            EntryNum{indAge,indParam,indCond} = cell2mat(cellfun(@str2num,EntryNum{indAge,indParam,indCond},'un',0));
            MeanValuesMRI(indParam,indCond,EntryNum{indAge,indParam,indCond}) = nanmean(arrayData(:,Indices),1);
        end
    end
end
clear EntryNum Indices

%% plot mean effects on HDDM parameters

condPairs = [1,2; 2,3; 3,4];
condPairsLevel{1} = [2.8, 1.75, 1.35];  
condPairsLevel{2} = [1.6, 1.75, 2];  
condPairsLevel{3} = [.4, .5, .55];  
colorm = [0.0314, 0.3176, 0.6118; .5,.5,.5; 2.*[.3 .1 .1]];
paramLabels = {'Drift rate'; 'Threshold'; 'Non-decision time'}; 

h = figure('units','normalized','position',[.1 .1 .15 .6]);
for indIdx = 1:3
    subplot(3,1,indIdx)
    cla; hold on;

    curData = squeeze(permute(MeanValuesMRI(indIdx,:,idx_OA),[3,1,2]));
    
 % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = squeeze(curData(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = curData(:,i)-...
                nanmean(curData(:,:),2)+...
                repmat(nanmean(nanmean(curData(:,:),2),1),size(curData(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = colorm(indIdx,:);

    box off
    cla;
        h_rc = rm_raincloud(data_ws, cl,1);
        % add stats
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
               mysigstar_vert(gca, [condPairsLevel{indIdx}(indPair), condPairsLevel{indIdx}(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

            end
        end
        view([90 -90]);
        axis ij
    box(gca,'off')
    %set(gca, 'YTick', [1,2,3,4]);
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    ylabel('Target load'); xlabel(paramLabels{indIdx})
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    xlim(lims{indIdx}); 
    curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);
end

set(findall(gcf,'-property','FontSize'),'FontSize',22)
figureName = 'C_meanEffects_MRI_OA_vt_RCP';
saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');


%% collect DDM estimates in a structure

HDDM_summary.thresholdMRI = squeeze(permute(MeanValuesMRI(2,:,:),[3,2,1]));
HDDM_summary.nondecisionMRI = squeeze(permute(MeanValuesMRI(3,:,:),[3,2,1]));
HDDM_summary.driftMRI = squeeze(permute(MeanValuesMRI(1,:,:),[3,2,1]));

HDDM_summary.thresholdEEG = squeeze(permute(MeanValuesEEG(2,:,:),[3,2,1]));
HDDM_summary.nondecisionEEG = squeeze(permute(MeanValuesEEG(3,:,:),[3,2,1]));
HDDM_summary.driftEEG = squeeze(permute(MeanValuesEEG(1,:,:),[3,2,1]));

HDDM_summary.IDs = IDs_all;

save(fullfile(pn.data, 'HDDM_summary_OA_vt.mat'), 'HDDM_summary')

%% plot reliability

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(4,'RdBu');

% add convertPtoExponential
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/')

h = figure('units','normalized','position',[.1 .1 .35 .35]);
p = []; r = []; y_ls = [];
subplot(3,3,[1,4]); cla;
    title('Threshold (T.)'); hold on;
    for indCond = 1
        x1 = MeanValuesEEG(2,indCond,idx_OA);
        y1 = MeanValuesMRI(2,indCond,idx_OA);
        x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
        y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
        scatter(x, y, 'filled', 'MarkerFaceColor', [0 0 0]);
        y_ls{indCond} = polyval(polyfit(x,y,1),x); y_ls{indCond} = plot(x, y_ls{indCond}, 'Color', [0 0 0], 'LineWidth', 1);
        [r{indCond}, p{indCond}] = corrcoef(x,y, 'rows', 'complete'); p{indCond} = convertPtoExponential(p{indCond}(2));
    end
    xlabel('T. EEG Session'); ylabel('Threshold MRI session');
    lg1 = legend([y_ls{1}], {['r = ', num2str(round(r{1}(2),2)), ', p = ' p{1}{1}]}, 'location', 'South'); legend('boxoff');
    ylim([-0 2]); xlim([.8 2])
subplot(3,3,[2,5]); cla;
    title('Non-decision time (NDT)'); hold on;
    for indCond = 1:4
        x1 = MeanValuesEEG(3,indCond,idx_OA);
        y1 = MeanValuesMRI(3,indCond,idx_OA);
        x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
        y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
        scatter(x, y, 'filled', 'MarkerFaceColor', cBrew(indCond,:));
        y_ls{indCond} = polyval(polyfit(x,y,1),x); y_ls{indCond} = plot(x, y_ls{indCond}, 'Color', cBrew(indCond,:), 'LineWidth', 1);
        [r{indCond}, p{indCond}] = corrcoef(x,y, 'rows', 'complete'); p{indCond} = convertPtoExponential(p{indCond}(2));
    end
    lg2 = legend([y_ls{1}, y_ls{2}, y_ls{3}, y_ls{4}], {['L1: r = ', num2str(round(r{1}(2),2)), ', p = ' p{1}{1}], ...
            ['L2: r = ', num2str(round(r{2}(2),2)), ', p = ' p{2}{1}],...
            ['L3: r = ', num2str(round(r{3}(2),2)), ', p = ' p{3}{1}],...
            ['L4: r = ', num2str(round(r{4}(2),2)), ', p = ' p{4}{1}]}, 'location', 'South'); legend('boxoff');
    xlabel('NDT EEG Session'); ylabel('NDT MRI session');
    ylim([-0 .6])
subplot(3,3,[3,6]); cla;
    title('Drift rate'); hold on;
    for indCond = 1:4
        x1 = MeanValuesEEG(1,indCond,idx_OA);
        y1 = MeanValuesMRI(1,indCond,idx_OA);
        x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
        y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
        scatter(x, y, 'filled', 'MarkerFaceColor', cBrew(indCond,:));
        y_ls{indCond} = polyval(polyfit(x,y,1),x); y_ls{indCond} = plot(x, y_ls{indCond}, 'Color', cBrew(indCond,:), 'LineWidth', 1);
        [r{indCond}, p{indCond}] = corrcoef(x,y, 'rows', 'complete'); p{indCond} = convertPtoExponential(p{indCond}(2));
    end
    xlabel('Drift EEG Session'); ylabel('Drift MRI session');
    lg3 = legend([y_ls{1}, y_ls{2}, y_ls{3}, y_ls{4}], {['L1: r = ', num2str(round(r{1}(2),2)), ', p = ' p{1}{1}], ...
            ['L2: r = ', num2str(round(r{2}(2),2)), ', p = ' p{2}{1}],...
            ['L3: r = ', num2str(round(r{3}(2),2)), ', p = ' p{3}{1}],...
            ['L4: r = ', num2str(round(r{4}(2),2)), ', p = ' p{4}{1}]}, 'location', 'South'); legend('boxoff');
    ylim([-1.5 3.5])
    clear p r y_ls

subplot(3,3,8); cla;hold on;
    %title('L4-L1 NDT change'); hold on;
    x1 = nanmean(MeanValuesEEG(3,4,idx_OA),2)-MeanValuesEEG(3,1,idx_OA);
    y1 = nanmean(MeanValuesMRI(3,4,idx_OA),2)-MeanValuesMRI(3,1,idx_OA);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'NDT EEG Session', 'L4-L1 change'}); ylabel({'NDT MRI session';'L4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg5 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    ylim([-.4 .4]); xlim([-.1 .4])
subplot(3,3,9); cla; hold on;
    %title('L4-L1 Drift change');
    x1 = nanmean(MeanValuesEEG(1,4,idx_OA),2)-MeanValuesEEG(1,1,idx_OA);
    y1 = nanmean(MeanValuesMRI(1,4,idx_OA),2)-MeanValuesMRI(1,1,idx_OA);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift EEG Session', 'L4-L1 change'}); ylabel({'Drift MRI session';'L4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg6 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    ylim([-5 1]); xlim([-4 0])
set(findall(gcf,'-property','FontSize'),'FontSize',15)
set(lg1,'FontSize',12)
set(lg2,'FontSize',12)
set(lg3,'FontSize',12)
set(lg5,'FontSize',12)
set(lg6,'FontSize',12)

pn.plotFolder = [pn.root, 'C_figures/'];
figureName = 'C_HDDMreliability_vt';

saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% plot parameter inter-correlations (also with RT, Acc)

h = figure('units','normalized','position',[.1 .1 .35 .35]);

% intercept-intercept across parameters

subplot(3,3,1); cla; hold on;
    %title('L4-L1 Threshold change'); hold on;
    x1 = MeanValuesEEG(1,1,idx_OA);
    y1 = MeanValuesEEG(2,1,idx_OA);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'L1'}); ylabel({'Threshold';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');

subplot(3,3,2); cla; hold on;
    x1 = MeanValuesEEG(1,1,idx_OA);
    y1 = MeanValuesEEG(3,1,idx_OA);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'L1'}); ylabel({'NDT';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    
subplot(3,3,3); cla; hold on;
    x1 = MeanValuesEEG(2,1,idx_OA);
    y1 = MeanValuesEEG(3,1,idx_OA);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Threshold', 'L1'}); ylabel({'NDT';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    
% change-change correlation

subplot(3,3,4); cla; hold on;
    x1 = nanmean(MeanValuesEEG(1,2:4,idx_OA),2)-MeanValuesEEG(1,1,idx_OA);
    y1 = nanmean(MeanValuesEEG(3,2:4,idx_OA),2)-MeanValuesEEG(3,1,idx_OA);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'L2:4-L1 change'}); ylabel({'NDT';'L2:4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');

%% Intercept-Change correlations

h = figure('units','normalized','position',[.1 .1 .35 .35]);
 
 suptitle('Intercept-Change correlations')

 subplot(3,3,1); cla; hold on;
    x1 = nanmean(MeanValuesEEG(1,1,idx_OA),2)
    y1 = nanmean(MeanValuesEEG(1,2:4,idx_OA),2)-MeanValuesEEG(1,1,idx_OA);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'L1'}); ylabel({'Drift';'L2:4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');

 subplot(3,3,2); cla; hold on;
    x1 = nanmean(MeanValuesEEG(2,1,idx_OA),2)
    y1 = nanmean(MeanValuesEEG(1,2:4,idx_OA),2)-MeanValuesEEG(1,1,idx_OA);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Threshold', 'L1'}); ylabel({'Drift';'L2:4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');

  subplot(3,3,3); cla; hold on;
    x1 = nanmean(MeanValuesEEG(3,1,idx_OA),2)
    y1 = nanmean(MeanValuesEEG(1,2:4,idx_OA),2)-MeanValuesEEG(1,1,idx_OA);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'NDT', 'L1'}); ylabel({'Drift';'L2:4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');

    %    
  subplot(3,3,7); cla; hold on;
    x1 = nanmean(MeanValuesEEG(1,1,idx_OA),2)
    y1 = nanmean(MeanValuesEEG(3,2:4,idx_OA),2)-MeanValuesEEG(3,1,idx_OA);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'L1'}); ylabel({'NDT';'L2:4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');

 subplot(3,3,8); cla; hold on;
    x1 = nanmean(MeanValuesEEG(2,1,idx_OA),2)
    y1 = nanmean(MeanValuesEEG(3,2:4,idx_OA),2)-MeanValuesEEG(3,1,idx_OA);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Threshold', 'L1'}); ylabel({'NDT';'L2:4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');

  subplot(3,3,9); cla; hold on;
    x1 = nanmean(MeanValuesEEG(3,1,idx_OA),2)
    y1 = nanmean(MeanValuesEEG(3,2:4,idx_OA),2)-MeanValuesEEG(3,1,idx_OA);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'NDT', 'L1'}); ylabel({'NDT';'L2:4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');

pn.plotFolder = [pn.root, 'C_figures/'];
figureName = 'C_Interrelations_interceptChange_vt';

saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');


%% Correlations with Accuracy and RT
% intercept & change-change

load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat'])

idxSummaryIDs = find(ismember(STSWD_summary.IDs, IDs_all));

h = figure('units','normalized','position',[.1 .1 .35 .3]);
 
 %suptitle('Intercept-Intercept correlations')

 subplot(2,3,1); cla; hold on;
    x1 = squeeze(nanmean(MeanValuesEEG(1,1,idx_OA),2));
    y1 = STSWD_summary.behav.EEGRT(idxSummaryIDs(idx_OA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'L1'}); ylabel({'RT';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'NorthEast'); legend('boxoff');
 subplot(2,3,2); cla; hold on;
    x1 = squeeze(nanmean(MeanValuesEEG(2,1,idx_OA),2));
    y1 = STSWD_summary.behav.EEGRT(idxSummaryIDs(idx_OA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Threshold', 'L1'}); ylabel({'RT';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'NorthWest'); legend('boxoff');
 subplot(2,3,3); cla; hold on;
    x1 = squeeze(nanmean(MeanValuesEEG(3,1,idx_OA),2));
    y1 = STSWD_summary.behav.EEGRT(idxSummaryIDs(idx_OA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'NDT', 'L1'}); ylabel({'RT';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'NorthEast'); legend('boxoff');

 subplot(2,3,4); cla; hold on;
    x1 = squeeze(nanmean(MeanValuesEEG(1,1,idx_OA),2));
    y1 = STSWD_summary.behav.EEGAcc(idxSummaryIDs(idx_OA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'L1'}); ylabel({'Accuracy';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
 subplot(2,3,5); cla; hold on;
    x1 = squeeze(nanmean(MeanValuesEEG(2,1,idx_OA),2));
    y1 = STSWD_summary.behav.EEGAcc(idxSummaryIDs(idx_OA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Threshold', 'L1'}); ylabel({'Accuracy';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
 subplot(2,3,6); cla; hold on;
    x1 = squeeze(nanmean(MeanValuesEEG(3,1,idx_OA),2));
    y1 = STSWD_summary.behav.EEGAcc(idxSummaryIDs(idx_OA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'NDT', 'L1'}); ylabel({'Accuracy';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');

    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
pn.plotFolder = [pn.root, 'C_figures/'];
figureName = 'C_RTACC_bl_vt';

saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');


h = figure('units','normalized','position',[.1 .1 .35 .3]);
 
 %suptitle('Change-change correlations')

 subplot(2,3,1); cla; hold on;
    x1 = squeeze(nanmean(MeanValuesEEG(1,2:4,idx_OA),2))-squeeze(nanmean(MeanValuesEEG(1,1,idx_OA),2));
    y1 = nanmean(STSWD_summary.behav.EEGRT(idxSummaryIDs(idx_OA),2:4),2)-nanmean(STSWD_summary.behav.EEGRT(idxSummaryIDs(idx_OA),1),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'L1'}); ylabel({'RT';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
 subplot(2,3,2); cla; hold on;
    x1 = squeeze(nanmean(MeanValuesEEG(2,2:4,idx_OA),2))-squeeze(nanmean(MeanValuesEEG(2,1,idx_OA),2));
    y1 = nanmean(STSWD_summary.behav.EEGRT(idxSummaryIDs(idx_OA),2:4),2)-nanmean(STSWD_summary.behav.EEGRT(idxSummaryIDs(idx_OA),1),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Threshold', 'L1'}); ylabel({'RT';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
 subplot(2,3,3); cla; hold on;
    x1 = squeeze(nanmean(MeanValuesEEG(3,2:4,idx_OA),2))-squeeze(nanmean(MeanValuesEEG(3,1,idx_OA),2));
    y1 = nanmean(STSWD_summary.behav.EEGRT(idxSummaryIDs(idx_OA),2:4),2)-nanmean(STSWD_summary.behav.EEGRT(idxSummaryIDs(idx_OA),1),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'NDT', 'L1'}); ylabel({'RT';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');

 subplot(2,3,4); cla; hold on;
   x1 = squeeze(nanmean(MeanValuesEEG(1,2:4,idx_OA),2))-squeeze(nanmean(MeanValuesEEG(1,1,idx_OA),2));
    y1 = nanmean(STSWD_summary.behav.EEGAcc(idxSummaryIDs(idx_OA),2:4),2)-nanmean(STSWD_summary.behav.EEGAcc(idxSummaryIDs(idx_OA),1),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'L1'}); ylabel({'Accuracy';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
 subplot(2,3,5); cla; hold on;
    x1 = squeeze(nanmean(MeanValuesEEG(2,2:4,idx_OA),2))-squeeze(nanmean(MeanValuesEEG(2,1,idx_OA),2));
    y1 = nanmean(STSWD_summary.behav.EEGAcc(idxSummaryIDs(idx_OA),2:4),2)-nanmean(STSWD_summary.behav.EEGAcc(idxSummaryIDs(idx_OA),1),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Threshold', 'L1'}); ylabel({'Accuracy';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
 subplot(2,3,6); cla; hold on;
    x1 = squeeze(nanmean(MeanValuesEEG(3,2:4,idx_OA),2))-squeeze(nanmean(MeanValuesEEG(3,1,idx_OA),2));
    y1 = nanmean(STSWD_summary.behav.EEGAcc(idxSummaryIDs(idx_OA),2:4),2)-nanmean(STSWD_summary.behav.EEGAcc(idxSummaryIDs(idx_OA),1),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'NDT', 'L1'}); ylabel({'Accuracy';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');

    set(findall(gcf,'-property','FontSize'),'FontSize',14)

pn.plotFolder = [pn.root, 'C_figures/'];
figureName = 'C_RTACC_change_vt';

saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');
