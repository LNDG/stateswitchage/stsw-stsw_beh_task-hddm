% Calculate reliability of HDDM estimates

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data = fullfile(pn.root, 'data');
pn.plotFolder = fullfile(pn.root, 'figures');
    addpath(genpath(fullfile(pn.root, 'tools', 'RainCloudPlots')));
    addpath(fullfile(pn.root, 'tools'));
pn.summary = fullfile(pn.root, '..', '..', 'stsw_multimodal', 'data');

load(fullfile(pn.summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

filename = fullfile(rootpath, 'code', 'id_list_eegmr.txt');
fileID = fopen(filename);
IDs_EEGMR = textscan(fileID,'%s');
fclose(fileID);
IDs_EEGMR = IDs_EEGMR{1};

IDs_age{1} = cellfun(@str2num, IDs_EEGMR)<2000;
IDs_age{2} = cellfun(@str2num, IDs_EEGMR)>2000;

%% plot using RCPs

condPairs = [1,2; 2,3; 3,4];
condPairsLevel = [];
condPairsLevel{1} = [2.2,2.0, 1.8];  
condPairsLevel{2} = [1.8, 1.9, 2];  
condPairsLevel{3} = [.45, .5, .55]; 
lims{1} = [-.5 2.5]; lims{2} = [0.5 2]; lims{3} = [0 .6];
colorm = [0.0314, 0.3176, 0.6118; 2.*[.3 .1 .1]; 2.*[.3 .1 .1]];
paramLabels = {'Drift rate'; 'Threshold'; 'Non-decision time'}; 
fields = {'driftEEGMRI'; 'thresholdEEGMRI'; 'nondecisionEEGMRI'}; 

for indField = 1:3
    
    if indField == 1
        cBrew(1,:) = 2.*[.1 .1 .3];
        cBrew(2,:) = 2.*[.1 .1 .3];
    else
        cBrew(1,:) = 2.*[.3 .1 .1];
        cBrew(2,:) = 2.*[.3 .1 .1];
    end

    idx_outlier = cell(1); idx_standard = cell(1);
    for indGroup = 1:2
        dataToPlot = STSWD_summary.HDDM_vat.(fields{indField})(IDs_age{indGroup},:);
        % define outlier as lin. modulation that is more than three scaled median absolute deviations (MAD) away from the median
        X = [1 1; 1 2; 1 3; 1 4]; b=X\dataToPlot'; IndividualSlopes = b(2,:);
        outliers = isoutlier(IndividualSlopes, 'median');
        idx_outlier{indGroup} = find(outliers);
        idx_standard{indGroup} = find(outliers==0);
    end


    h = figure('units','centimeter','position',[0 0 18 7]);
    for indGroup = 1:2
        dataToPlot = STSWD_summary.HDDM_vat.(fields{indField})(IDs_age{indGroup},:);
        % read into cell array of the appropriate dimensions
        data = []; data_ws = [];
        for i = 1:4
            for j = 1:1
                data{i, j} = dataToPlot(:,i);
                % individually demean for within-subject visualization
                data_ws{i, j} = dataToPlot(:,i)-...
                    nanmean(dataToPlot(:,:),2)+...
                    repmat(nanmean(nanmean(dataToPlot(:,:),2),1),size(dataToPlot(:,:),1),1);
                data_nooutlier{i, j} = data{i, j};
                data_nooutlier{i, j}(idx_outlier{indGroup}) = [];
                data_ws_nooutlier{i, j} = data_ws{i, j};
                data_ws_nooutlier{i, j}(idx_outlier{indGroup}) = [];
                % sort outliers to back in original data for improved plot overlap
                data_ws{i, j} = [data_ws{i, j}(idx_standard{indGroup}); data_ws{i, j}(idx_outlier{indGroup})];
            end
        end

        % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

        subplot(1,2,indGroup);
        set(gcf,'renderer','Painters')
            cla;
            cl = cBrew(indGroup,:);
            %rm_raincloud_fixedSpacing(data_ws, [.8 .8 .8],1,[],[],[],15);
            h_rc = rm_raincloud(data_ws_nooutlier, cl,1);
            view([90 -90]);
            axis ij
        box(gca,'off')
        set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
        yticks = get(gca, 'ytick'); ylim([yticks(1)-(yticks(2)-yticks(1))./2, yticks(4)+(yticks(2)-yticks(1))./2]);

        minmax = [min(min(cat(2,data_ws{:}))), max(max(cat(2,data_ws{:})))];
        xlim(minmax+[-0.2*diff(minmax), 0.2*diff(minmax)])
        ylabel('Target load'); xlabel(paramLabels{indField})

        % test linear effect
        curData = [data_nooutlier{1, 1}, data_nooutlier{2, 1}, data_nooutlier{3, 1}, data_nooutlier{4, 1}];
        X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
        [~, p, ci, stats] = ttest(IndividualSlopes);
        title(['M:', num2str(round(mean(IndividualSlopes),3)), '; p=', num2str(round(p,3))])
    end
    set(findall(gcf,'-property','FontSize'),'FontSize',22)
    figureName = ['c_meanEffects_yaoa_vat_', paramLabels{indField}];
    saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
    saveas(h, fullfile(pn.plotFolder, figureName), 'png');
end
