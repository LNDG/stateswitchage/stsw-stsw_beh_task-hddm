%% assess the reliability of HDDM-derived estimates and paramater intercorrelations

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data = fullfile(pn.root, 'data');
pn.plotFolder = fullfile(pn.root, 'figures');
addpath(genpath(fullfile(pn.root, 'tools', 'RainCloudPlots')));
addpath(fullfile(pn.root, 'tools', 'BrewerMap')); cBrew = brewermap(4,'RdBu');
addpath(fullfile(pn.root, 'tools'));

% load merged data matrix
load(fullfile(pn.data, 'HDDM_summary_YA_vt.mat'), 'HDDM_summary')

load(fullfile(pn.root, '..', '..', 'stsw_multimodal', 'data', 'STSWD_summary_YAOA.mat'))

idx_YA = cellfun(@str2num, HDDM_summary.IDs)<2000;
idx_OA = cellfun(@str2num, HDDM_summary.IDs)>2000;

idxSummaryIDs = find(ismember(STSWD_summary.IDs, HDDM_summary.IDs(idx_YA)));

%% plot reliability

h = figure('units','normalized','position',[.1 .1 .35 .35]);
p = []; r = []; y_ls = [];
subplot(3,3,[1,4]); cla;
    title('Threshold (T.)'); hold on;
    for indCond = 1
        x1 = HDDM_summary.thresholdEEG(idx_YA,indCond);
        y1 = HDDM_summary.thresholdMRI(idx_YA,indCond);
        x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
        y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
        scatter(x, y, 'filled', 'MarkerFaceColor', [0 0 0]);
        y_ls{indCond} = polyval(polyfit(x,y,1),x); y_ls{indCond} = plot(x, y_ls{indCond}, 'Color', [0 0 0], 'LineWidth', 1);
        [r{indCond}, p{indCond}] = corrcoef(x,y, 'rows', 'complete'); p{indCond} = convertPtoExponential(p{indCond}(2));
    end
    xlabel('T. EEG Session'); ylabel('Threshold MRI session');
    lg1 = legend([y_ls{1}], {['r = ', num2str(round(r{1}(2),2)), ', p = ' p{1}{1}]}, 'location', 'South'); legend('boxoff');
    ylim([-0 2]); xlim([.8 2])
subplot(3,3,[2,5]); cla;
    title('Non-decision time (NDT)'); hold on;
    for indCond = 1:4
        x1 = HDDM_summary.nondecisionEEG(idx_YA,indCond);
        y1 = HDDM_summary.nondecisionMRI(idx_YA,indCond);
        x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
        y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
        scatter(x, y, 'filled', 'MarkerFaceColor', cBrew(indCond,:));
        y_ls{indCond} = polyval(polyfit(x,y,1),x); y_ls{indCond} = plot(x, y_ls{indCond}, 'Color', cBrew(indCond,:), 'LineWidth', 1);
        [r{indCond}, p{indCond}] = corrcoef(x,y, 'rows', 'complete'); p{indCond} = convertPtoExponential(p{indCond}(2));
    end
    lg2 = legend([y_ls{1}, y_ls{2}, y_ls{3}, y_ls{4}], {['L1: r = ', num2str(round(r{1}(2),2)), ', p = ' p{1}{1}], ...
            ['L2: r = ', num2str(round(r{2}(2),2)), ', p = ' p{2}{1}],...
            ['L3: r = ', num2str(round(r{3}(2),2)), ', p = ' p{3}{1}],...
            ['L4: r = ', num2str(round(r{4}(2),2)), ', p = ' p{4}{1}]}, 'location', 'South'); legend('boxoff');
    xlabel('NDT EEG Session'); ylabel('NDT MRI session');
    ylim([-0 .6])
subplot(3,3,[3,6]); cla;
    title('Drift rate'); hold on;
    for indCond = 1:4
        x1 = HDDM_summary.driftEEG(idx_YA,indCond);
        y1 = HDDM_summary.driftMRI(idx_YA,indCond);
        x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
        y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
        scatter(x, y, 'filled', 'MarkerFaceColor', cBrew(indCond,:));
        y_ls{indCond} = polyval(polyfit(x,y,1),x); y_ls{indCond} = plot(x, y_ls{indCond}, 'Color', cBrew(indCond,:), 'LineWidth', 1);
        [r{indCond}, p{indCond}] = corrcoef(x,y, 'rows', 'complete'); p{indCond} = convertPtoExponential(p{indCond}(2));
    end
    xlabel('Drift EEG Session'); ylabel('Drift MRI session');
    lg3 = legend([y_ls{1}, y_ls{2}, y_ls{3}, y_ls{4}], {['L1: r = ', num2str(round(r{1}(2),2)), ', p = ' p{1}{1}], ...
            ['L2: r = ', num2str(round(r{2}(2),2)), ', p = ' p{2}{1}],...
            ['L3: r = ', num2str(round(r{3}(2),2)), ', p = ' p{3}{1}],...
            ['L4: r = ', num2str(round(r{4}(2),2)), ', p = ' p{4}{1}]}, 'location', 'South'); legend('boxoff');
    ylim([-1.5 3.5])
    clear p r y_ls
%%%
% --- no threshold modulation here
%%%
subplot(3,3,8); cla;hold on;
    x1 = HDDM_summary.nondecisionEEG(idx_YA,4)-HDDM_summary.nondecisionEEG(idx_YA,1);
    y1 = HDDM_summary.nondecisionMRI(idx_YA,4)-HDDM_summary.nondecisionMRI(idx_YA,1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'NDT EEG Session', 'L4-L1 change'}); ylabel({'NDT MRI session';'L4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg5 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    ylim([-.4 .4]); xlim([-.1 .4])
subplot(3,3,9); cla; hold on;
    x1 = HDDM_summary.driftEEG(idx_YA,4)-HDDM_summary.driftEEG(idx_YA,1);
    y1 = HDDM_summary.driftMRI(idx_YA,4)-HDDM_summary.driftMRI(idx_YA,1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift EEG Session', 'L4-L1 change'}); ylabel({'Drift MRI session';'L4-L1 change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg6 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    ylim([-5 1]); xlim([-4 0])
set(findall(gcf,'-property','FontSize'),'FontSize',15)
set(lg1,'FontSize',12)
set(lg2,'FontSize',12)
set(lg3,'FontSize',12)
set(lg5,'FontSize',12)
set(lg6,'FontSize',12)

figureName = 'C_HDDMreliability_vt';

saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% plot relaibility in single plot

h = figure('units','normalized','position',[.1 .1 .35 .25]);
set(0, 'DefaultFigureRenderer', 'painters'); hold on;
p = []; r = []; y_ls = [];
ax1 = subplot(1,2,[1]); cla; hold on;
    %title('Non-decision time (NDT)'); hold on;
    x1 = nanmean(STSWD_summary.HDDM_vt.nondecisionEEG(idxSummaryIDs,1),2);
    y1 = nanmean(STSWD_summary.HDDM_vt.nondecisionMRI(idxSummaryIDs,1),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    SourceData_EEG_NDT = x; SourceData_MRI_NDT = y;
    scatter(x, y, 'filled', 'MarkerFaceColor', cBrew(1,:));
    y_ls{1} = polyval(polyfit(x,y,1),x); y_ls{1} = plot(x, y_ls{1}, 'Color', cBrew(1,:), 'LineWidth', 2);
    [r{1}, p{1}] = corrcoef(x,y, 'rows', 'complete'); p{1} = convertPtoExponential(p{1}(2));
    %lg2 = legend([y_ls{1}], {['L1: r = ', num2str(round(r{1}(2),2)), ', p = ' p{1}{1}]}, 'location', 'South'); legend('boxoff');
    xlabel('NDT EEG Session'); ylabel('NDT MRI session');
    
    ax1_pos = ax1.Position;
    ax2 = axes('Position',ax1_pos,...
    'XAxisLocation','top',...
    'YAxisLocation','right', 'Color', 'none');

    hold on;
    x1 = nanmean(STSWD_summary.HDDM_vt.nondecisionEEG_linear(idxSummaryIDs,1),2);
    y1 = nanmean(STSWD_summary.HDDM_vt.nondecisionMRI_linear(idxSummaryIDs,1),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    SourceData_EEG_NDTlin = x; SourceData_MRI_NDTlin = y;
    scatter(x, y, 'Parent', ax2, 'filled', 'MarkerFaceColor', cBrew(4,:));
    y_ls{2} = polyval(polyfit(x,y,1),x); y_ls{2} = plot(x, y_ls{2}, 'Parent', ax2, 'Color', cBrew(4,:), 'LineWidth', 2);
    [r{2}, p{2}] = corrcoef(x,y, 'rows', 'complete'); p{2} = convertPtoExponential(p{2}(2));
    lg2 = legend([y_ls{1}, y_ls{2}], {['L1: r = ', num2str(round(r{1}(2),2)), ', p = ' p{1}{1}],...
        ['linear: r = ', num2str(round(r{2}(2),2)), ', p = ' p{2}{1}]}, 'location', 'SouthEast');

ax1 = subplot(1,2,[2]); cla; hold on;
    %title('Non-decision time (NDT)'); hold on;
    x1 = nanmean(STSWD_summary.HDDM_vt.driftEEG(idxSummaryIDs,1),2);
    y1 = nanmean(STSWD_summary.HDDM_vt.driftMRI(idxSummaryIDs,1),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    SourceData_EEG_Drift = x; SourceData_MRI_Drift = y;
    scatter(x, y, 'filled', 'MarkerFaceColor', cBrew(1,:));
    y_ls{1} = polyval(polyfit(x,y,1),x); y_ls{1} = plot(x, y_ls{1}, 'Color', cBrew(1,:), 'LineWidth', 2);
    [r{1}, p{1}] = corrcoef(x,y, 'rows', 'complete'); p{1} = convertPtoExponential(p{1}(2));
    %lg2 = legend([y_ls{1}], {['L1: r = ', num2str(round(r{1}(2),2)), ', p = ' p{1}{1}]}, 'location', 'South'); legend('boxoff');
    xlabel('Drift EEG Session (1 Target)'); ylabel('Drift MRI session (1 Target)');
    
    ax1_pos = ax1.Position;
    ax2 = axes('Position',ax1_pos,...
    'XAxisLocation','top',...
    'YAxisLocation','right', 'Color', 'none');

    hold on;
    x1 = nanmean(STSWD_summary.HDDM_vt.driftEEG_linear(idxSummaryIDs,1),2);
    y1 = nanmean(STSWD_summary.HDDM_vt.driftMRI_linear(idxSummaryIDs,1),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    SourceData_EEG_Driftlin = x; SourceData_MRI_Driftlin = y;
    scatter(x, y, 'Parent', ax2, 'filled', 'MarkerFaceColor', cBrew(4,:));
    y_ls{2} = polyval(polyfit(x,y,1),x); y_ls{2} = plot(x, y_ls{2}, 'Parent', ax2, 'Color', cBrew(4,:), 'LineWidth', 2);
    [r{2}, p{2}] = corrcoef(x,y, 'rows', 'complete'); p{2} = convertPtoExponential(p{2}(2));
    lg2 = legend([y_ls{1}, y_ls{2}], {['L1: r = ', num2str(round(r{1}(2),2)), ', p = ' p{1}{1}],...
        ['linear: r = ', num2str(round(r{2}(2),2)), ', p = ' p{2}{1}]}, 'location', 'SouthEast');
set(findall(gcf,'-property','FontSize'),'FontSize',15)

figureName = 'C_HDDMreliability_vt_selected';

saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% SourceData

SourceData = [SourceData_EEG_Drift,...
SourceData_MRI_Drift,...
SourceData_EEG_Driftlin,...
SourceData_MRI_Driftlin,...
SourceData_EEG_NDT,...
SourceData_MRI_NDT,...
SourceData_EEG_NDTlin,...
SourceData_MRI_NDTlin];

%% within-subject stability

h = figure('units','normalized','position',[.1 .1 .35 .25]);
set(0, 'DefaultFigureRenderer', 'painters'); hold on;
p = []; r = []; y_ls = [];
ax1 = subplot(1,2,[1]); cla; hold on;
    %title('Non-decision time (NDT)'); hold on;
    x1 = nanmean(STSWD_summary.HDDM_vt.nondecisionEEG(idxSummaryIDs,1),2);
    y1 = nanmean(STSWD_summary.HDDM_vt.nondecisionEEG(idxSummaryIDs,4),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'MarkerFaceColor', cBrew(1,:));
    y_ls{1} = polyval(polyfit(x,y,1),x); y_ls{1} = plot(x, y_ls{1}, 'Color', cBrew(1,:), 'LineWidth', 2);
    [r{1}, p{1}] = corrcoef(x,y, 'rows', 'complete'); p{1} = convertPtoExponential(p{1}(2));
    %lg2 = legend([y_ls{1}], {['L1: r = ', num2str(round(r{1}(2),2)), ', p = ' p{1}{1}]}, 'location', 'South'); legend('boxoff');
    xlabel('L1 NDT EEG Session'); ylabel('L4 NDT');
    
    ax1_pos = ax1.Position;
    ax2 = axes('Position',ax1_pos,...
    'XAxisLocation','top',...
    'YAxisLocation','right', 'Color', 'none');

    hold on;
    x1 = nanmean(STSWD_summary.HDDM_vt.nondecisionMRI(idxSummaryIDs,1),2);
    y1 = nanmean(STSWD_summary.HDDM_vt.nondecisionMRI(idxSummaryIDs,4),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'Parent', ax2, 'filled', 'MarkerFaceColor', cBrew(4,:));
    y_ls{2} = polyval(polyfit(x,y,1),x); y_ls{2} = plot(x, y_ls{2}, 'Parent', ax2, 'Color', cBrew(4,:), 'LineWidth', 2);
    [r{2}, p{2}] = corrcoef(x,y, 'rows', 'complete'); p{2} = convertPtoExponential(p{2}(2));
    lg2 = legend([y_ls{1}, y_ls{2}], {['L1: r = ', num2str(round(r{1}(2),2)), ', p = ' p{1}{1}],...
        ['linear: r = ', num2str(round(r{2}(2),2)), ', p = ' p{2}{1}]}, 'location', 'SouthEast');

ax1 = subplot(1,2,[2]); cla; hold on;
    %title('Non-decision time (NDT)'); hold on;
    x1 = nanmean(STSWD_summary.HDDM_vt.driftEEG(idxSummaryIDs,1),2);
    y1 = nanmean(STSWD_summary.HDDM_vt.driftEEG(idxSummaryIDs,4),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'MarkerFaceColor', cBrew(1,:));
    y_ls{1} = polyval(polyfit(x,y,1),x); y_ls{1} = plot(x, y_ls{1}, 'Color', cBrew(1,:), 'LineWidth', 2);
    [r{1}, p{1}] = corrcoef(x,y, 'rows', 'complete'); p{1} = convertPtoExponential(p{1}(2));
    %lg2 = legend([y_ls{1}], {['L1: r = ', num2str(round(r{1}(2),2)), ', p = ' p{1}{1}]}, 'location', 'South'); legend('boxoff');
    xlabel('L1 Drfit EEG Session'); ylabel('L4 NDT');
    
    ax1_pos = ax1.Position;
    ax2 = axes('Position',ax1_pos,...
    'XAxisLocation','top',...
    'YAxisLocation','right', 'Color', 'none');

    hold on;
    x1 = nanmean(STSWD_summary.HDDM_vt.driftMRI(idxSummaryIDs,1),2);
    y1 = nanmean(STSWD_summary.HDDM_vt.driftMRI(idxSummaryIDs,4),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'Parent', ax2, 'filled', 'MarkerFaceColor', cBrew(4,:));
    y_ls{2} = polyval(polyfit(x,y,1),x); y_ls{2} = plot(x, y_ls{2}, 'Parent', ax2, 'Color', cBrew(4,:), 'LineWidth', 2);
    [r{2}, p{2}] = corrcoef(x,y, 'rows', 'complete'); p{2} = convertPtoExponential(p{2}(2));
    lg2 = legend([y_ls{1}, y_ls{2}], {['L1: r = ', num2str(round(r{1}(2),2)), ', p = ' p{1}{1}],...
        ['linear: r = ', num2str(round(r{2}(2),2)), ', p = ' p{2}{1}]}, 'location', 'SouthEast');
set(findall(gcf,'-property','FontSize'),'FontSize',15)


%% inter-individual behavior correlation L1 - L4

h = figure('units','normalized','position',[.1 .1 .35 .15]);
p = []; r = []; y_ls = [];
subplot(1,3,2); cla;hold on;
    x1 = HDDM_summary.nondecisionEEG(idx_YA,1);
    y1 = HDDM_summary.nondecisionEEG(idx_YA,4);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'NDT L1'}); ylabel({'NDT L4'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    
    x1 = HDDM_summary.nondecisionMRI(idx_YA,1);
    y1 = HDDM_summary.nondecisionMRI(idx_YA,4);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'r');
    y_ls_MRI = polyval(polyfit(x,y,1),x); y_ls_MRI = plot(x, y_ls_MRI, 'Color', [1 0 0], 'LineWidth', 1);
    [r_MRI, p_MRI] = corrcoef(x,y, 'rows', 'complete'); p_MRI = convertPtoExponential(p_MRI(2));
    lg5 = legend([y_ls, y_ls_MRI], {['EEG: r = ', num2str(round(r(2),2)), ', p = ' p{1}];...
        ['MRI: r = ', num2str(round(r_MRI(2),2)), ', p = ' p_MRI{1}]}, 'location', 'South'); legend('boxoff');
    ylim([.1 .6]); xlim([.1 .4])
subplot(1,3,3); cla; hold on;
    x1 = HDDM_summary.driftEEG(idx_YA,1);
    y1 = HDDM_summary.driftEEG(idx_YA,4);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift L1'}); ylabel({'Drift L4'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    
    x1 = HDDM_summary.driftMRI(idx_YA,1);
    y1 = HDDM_summary.driftMRI(idx_YA,4);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'r');
    y_ls_MRI = polyval(polyfit(x,y,1),x); y_ls_MRI = plot(x, y_ls_MRI, 'Color', [1 0 0], 'LineWidth', 1);
    [r_MRI, p_MRI] = corrcoef(x,y, 'rows', 'complete'); p_MRI = convertPtoExponential(p_MRI(2));
    lg6 = legend([y_ls, y_ls_MRI], {['EEG: r = ', num2str(round(r(2),2)), ', p = ' p{1}];...
        ['MRI: r = ', num2str(round(r_MRI(2),2)), ', p = ' p_MRI{1}]}, 'location', 'South'); legend('boxoff');
    ylim([-1 2]); xlim([0 6])

set(findall(gcf,'-property','FontSize'),'FontSize',15)
set(lg5,'FontSize',12)
set(lg6,'FontSize',12)

figureName = 'C_HDDMreliability_vt_L14';

saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% plot parameter inter-correlations (also with RT, Acc)

h = figure('units','normalized','position',[.1 .1 .35 .35]);

% intercept-intercept across parameters

subplot(3,3,1); cla; hold on;
    x1 = HDDM_summary.driftEEG(idx_YA,1);
    y1 = HDDM_summary.thresholdEEG(idx_YA,1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'L1'}); ylabel({'Threshold';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    % add MRI
    x1 = HDDM_summary.driftMRI(idx_YA,1);
    y1 = HDDM_summary.thresholdMRI(idx_YA,1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'r');
    xlabel({'Drift', 'L1'}); ylabel({'Threshold';'L1'});
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');

    
subplot(3,3,2); cla; hold on;
    x1 = HDDM_summary.driftEEG(idx_YA,1);
    y1 = HDDM_summary.nondecisionEEG(idx_YA,1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'L1'}); ylabel({'NDT';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    % add MRI
    x1 = HDDM_summary.driftMRI(idx_YA,1);
    y1 = HDDM_summary.nondecisionMRI(idx_YA,1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');
    
subplot(3,3,3); cla; hold on;
    x1 = HDDM_summary.thresholdEEG(idx_YA,1);
    y1 = HDDM_summary.nondecisionEEG(idx_YA,1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Threshold', 'L1'}); ylabel({'NDT';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    % add MRI
    x1 = HDDM_summary.thresholdMRI(idx_YA,1);
    y1 = HDDM_summary.nondecisionMRI(idx_YA,1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');
    
% change-change correlation

subplot(3,3,4); cla; hold on;
    x1 = nanmean(HDDM_summary.driftEEG(idx_YA,2:4),2)-HDDM_summary.driftEEG(idx_YA,1);
    y1 = nanmean(HDDM_summary.nondecisionEEG(idx_YA,2:4),2)-HDDM_summary.nondecisionEEG(idx_YA,1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'linear change'}); ylabel({'NDT';'linear change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    % add MRI
    x1 = nanmean(HDDM_summary.driftMRI(idx_YA,2:4),2)-HDDM_summary.driftMRI(idx_YA,1);
    y1 = nanmean(HDDM_summary.nondecisionMRI(idx_YA,2:4),2)-HDDM_summary.nondecisionMRI(idx_YA,1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');

%% Intercept-Change correlations

h = figure('units','normalized','position',[.1 .1 .35 .35]);
 
 suptitle('Intercept-Change correlations')

 subplot(3,3,1); cla; hold on;
 
    x = nanmean(STSWD_summary.HDDM_vt.driftEEG(idxSummaryIDs,1),2);
    y = nanmean(STSWD_summary.HDDM_vt.driftEEG_linear(idxSummaryIDs,1),2);
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'L1'}); ylabel({'Drift';'linear change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    % add MRI
    x = nanmean(STSWD_summary.HDDM_vt.driftMRI(idxSummaryIDs,1),2);
    y = nanmean(STSWD_summary.HDDM_vt.driftMRI_linear(idxSummaryIDs,1),2);
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');

 subplot(3,3,2); cla; hold on;
    x = nanmean(STSWD_summary.HDDM_vt.thresholdEEG(idxSummaryIDs,1),2);
    y = nanmean(STSWD_summary.HDDM_vt.driftEEG_linear(idxSummaryIDs,1),2);
    scatter(x, y, 'filled', 'k');
    xlabel({'Threshold', 'L1'}); ylabel({'Drift';'linear change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    % add MRI
    x = nanmean(STSWD_summary.HDDM_vt.thresholdMRI(idxSummaryIDs,1),2);
    y = nanmean(STSWD_summary.HDDM_vt.driftMRI_linear(idxSummaryIDs,1),2);
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');

  subplot(3,3,3); cla; hold on;
    x = nanmean(STSWD_summary.HDDM_vt.nondecisionEEG(idxSummaryIDs,1),2);
    y = nanmean(STSWD_summary.HDDM_vt.driftEEG_linear(idxSummaryIDs,1),2);
    scatter(x, y, 'filled', 'k');
    xlabel({'NDT', 'L1'}); ylabel({'Drift';'linear change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    % add MRI
    x = nanmean(STSWD_summary.HDDM_vt.nondecisionMRI(idxSummaryIDs,1),2);
    y = nanmean(STSWD_summary.HDDM_vt.driftMRI_linear(idxSummaryIDs,1),2);
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');
    
%%%
% -- no threshold modulation here
%%%

  subplot(3,3,7); cla; hold on;
    x = nanmean(STSWD_summary.HDDM_vt.driftEEG(idxSummaryIDs,1),2);
    y = nanmean(STSWD_summary.HDDM_vt.nondecisionEEG_linear(idxSummaryIDs,1),2);
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'L1'}); ylabel({'NDT';'linear change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    % add MRI
    x = nanmean(STSWD_summary.HDDM_vt.driftMRI(idxSummaryIDs,1),2);
    y = nanmean(STSWD_summary.HDDM_vt.nondecisionMRI_linear(idxSummaryIDs,1),2);
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');
    
 subplot(3,3,8); cla; hold on;
    x = nanmean(STSWD_summary.HDDM_vt.thresholdEEG(idxSummaryIDs,1),2);
    y = nanmean(STSWD_summary.HDDM_vt.nondecisionEEG_linear(idxSummaryIDs,1),2);
    scatter(x, y, 'filled', 'k');
    xlabel({'Threshold', 'L1'}); ylabel({'NDT';'linear change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    % add MRI
    x = nanmean(STSWD_summary.HDDM_vt.thresholdMRI(idxSummaryIDs,1),2);
    y = nanmean(STSWD_summary.HDDM_vt.nondecisionMRI_linear(idxSummaryIDs,1),2);
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');

  subplot(3,3,9); cla; hold on;
    x = nanmean(STSWD_summary.HDDM_vt.nondecisionEEG(idxSummaryIDs,1),2);
    y = nanmean(STSWD_summary.HDDM_vt.nondecisionEEG_linear(idxSummaryIDs,1),2);
    scatter(x, y, 'filled', 'k');
    xlabel({'NDT', 'L1'}); ylabel({'NDT';'linear change'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    % add MRI
    x = nanmean(STSWD_summary.HDDM_vt.nondecisionMRI(idxSummaryIDs,1),2);
    y = nanmean(STSWD_summary.HDDM_vt.nondecisionMRI_linear(idxSummaryIDs,1),2);
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');

figureName = 'C_Interrelations_interceptChange_vt';

saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');


%% Correlations with Accuracy and RT
% intercept & change-change

h = figure('units','normalized','position',[.1 .1 .35 .3]);
 
 %suptitle('Intercept-Intercept correlations')

 subplot(2,3,1); cla; hold on;
    x1 = squeeze(nanmean(HDDM_summary.driftEEG(idx_YA,1),2));
    y1 = STSWD_summary.behav.EEGRT(idxSummaryIDs(idx_YA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'L1'}); ylabel({'RT';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'NorthEast'); legend('boxoff');
    % add MRI
    x1 = nanmean(HDDM_summary.driftMRI(idx_YA,1),2);
    y1 = STSWD_summary.behav.EEGRT(idxSummaryIDs(idx_YA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');
 subplot(2,3,2); cla; hold on;
    x1 = squeeze(nanmean(HDDM_summary.thresholdEEG(idx_YA,1),2));
    y1 = STSWD_summary.behav.EEGRT(idxSummaryIDs(idx_YA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Threshold', 'L1'}); ylabel({'RT';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'NorthWest'); legend('boxoff');
    % add MRI
    x1 = nanmean(HDDM_summary.thresholdMRI(idx_YA,1),2);
    y1 = STSWD_summary.behav.MRIRT(idxSummaryIDs(idx_YA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');
 subplot(2,3,3); cla; hold on;
    x1 = squeeze(nanmean(HDDM_summary.nondecisionEEG(idx_YA,1),2));
    y1 = STSWD_summary.behav.EEGRT(idxSummaryIDs(idx_YA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'NDT', 'L1'}); ylabel({'RT';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'NorthEast'); legend('boxoff');
    % add MRI
    x1 = nanmean(HDDM_summary.nondecisionMRI(idx_YA,1),2);
    y1 = STSWD_summary.behav.MRIRT(idxSummaryIDs(idx_YA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');
 subplot(2,3,4); cla; hold on;
    x1 = squeeze(nanmean(HDDM_summary.driftEEG(idx_YA,1),2));
    y1 = STSWD_summary.behav.EEGAcc(idxSummaryIDs(idx_YA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'L1'}); ylabel({'Accuracy';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    % add MRI
    x1 = nanmean(HDDM_summary.driftMRI(idx_YA,1),2);
    y1 = STSWD_summary.behav.MRIAcc(idxSummaryIDs(idx_YA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');
 subplot(2,3,5); cla; hold on;
    x1 = squeeze(nanmean(HDDM_summary.thresholdEEG(idx_YA,1),2));
    y1 = STSWD_summary.behav.EEGAcc(idxSummaryIDs(idx_YA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Threshold', 'L1'}); ylabel({'Accuracy';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    % add MRI
    x1 = nanmean(HDDM_summary.thresholdMRI(idx_YA,1),2);
    y1 = STSWD_summary.behav.MRIAcc(idxSummaryIDs(idx_YA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');
 subplot(2,3,6); cla; hold on;
    x1 = squeeze(nanmean(HDDM_summary.nondecisionEEG(idx_YA,1),2));
    y1 = STSWD_summary.behav.EEGAcc(idxSummaryIDs(idx_YA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'NDT', 'L1'}); ylabel({'Accuracy';'L1'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    % add MRI
    x1 = nanmean(HDDM_summary.nondecisionMRI(idx_YA,1),2);
    y1 = STSWD_summary.behav.MRIAcc(idxSummaryIDs(idx_YA),1);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');

    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
figureName = 'C_RTACC_bl_vt';

saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% Correlations with Accuracy and RT
% intercept & change-change

h = figure('units','normalized','position',[.1 .1 .35 .3]);
 
 %suptitle('Intercept-Intercept correlations')

 subplot(2,3,1); cla; hold on;
    x1 = squeeze(nanmean(HDDM_summary.driftEEG(idx_YA,1:4),2));
    y1 = nanmean(STSWD_summary.behav.EEGRT(idxSummaryIDs(idx_YA),1:4),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'avg'}); ylabel({'RT';'avg'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'NorthEast'); legend('boxoff');
    % add MRI
    x1 = nanmean(HDDM_summary.driftMRI(idx_YA,1:4),2);
    y1 = nanmean(STSWD_summary.behav.EEGRT(idxSummaryIDs(idx_YA),1:4),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');
 subplot(2,3,2); cla; hold on;
    x1 = squeeze(nanmean(HDDM_summary.thresholdEEG(idx_YA,1:4),2));
    y1 = nanmean(STSWD_summary.behav.EEGRT(idxSummaryIDs(idx_YA),1:4),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Threshold', 'avg'}); ylabel({'RT';'avg'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'NorthWest'); legend('boxoff');
    % add MRI
    x1 = nanmean(HDDM_summary.thresholdMRI(idx_YA,1:4),2);
    y1 = nanmean(STSWD_summary.behav.MRIRT(idxSummaryIDs(idx_YA),1:4),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');
 subplot(2,3,3); cla; hold on;
    x1 = squeeze(nanmean(HDDM_summary.nondecisionEEG(idx_YA,1:4),2));
    y1 = nanmean(STSWD_summary.behav.EEGRT(idxSummaryIDs(idx_YA),1:4),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'NDT', 'avg'}); ylabel({'RT';'avg'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'NorthEast'); legend('boxoff');
    % add MRI
    x1 = nanmean(HDDM_summary.nondecisionMRI(idx_YA,1:4),2);
    y1 = nanmean(STSWD_summary.behav.MRIRT(idxSummaryIDs(idx_YA),1:4),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');
 subplot(2,3,4); cla; hold on;
    x1 = squeeze(nanmean(HDDM_summary.driftEEG(idx_YA,1:4),2));
    y1 = nanmean(STSWD_summary.behav.EEGAcc(idxSummaryIDs(idx_YA),1:4),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'avg'}); ylabel({'Accuracy';'avg'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    % add MRI
    x1 = nanmean(HDDM_summary.driftMRI(idx_YA,1:4),2);
    y1 = nanmean(STSWD_summary.behav.MRIAcc(idxSummaryIDs(idx_YA),1:4),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');
 subplot(2,3,5); cla; hold on;
    x1 = squeeze(nanmean(HDDM_summary.thresholdEEG(idx_YA,1:4),2));
    y1 = nanmean(STSWD_summary.behav.EEGAcc(idxSummaryIDs(idx_YA),1:4),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Threshold', 'avg'}); ylabel({'Accuracy';'avg'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    % add MRI
    x1 = nanmean(HDDM_summary.thresholdMRI(idx_YA,1:4),2);
    y1 = nanmean(STSWD_summary.behav.MRIAcc(idxSummaryIDs(idx_YA),1:4),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');
 subplot(2,3,6); cla; hold on;
    x1 = squeeze(nanmean(HDDM_summary.nondecisionEEG(idx_YA,1:4),2));
    y1 = nanmean(STSWD_summary.behav.EEGAcc(idxSummaryIDs(idx_YA),1:4),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'NDT', 'avg'}); ylabel({'Accuracy';'avg'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    % add MRI
    x1 = nanmean(HDDM_summary.nondecisionMRI(idx_YA,1:4),2);
    y1 = nanmean(STSWD_summary.behav.MRIAcc(idxSummaryIDs(idx_YA),1:4),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');

    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
pn.plotFolder = [pn.root, 'C_figures/'];
figureName = 'C_RTACC_avg_vt';

saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% Figure: linear change-change associations with RT and Accuracy

h = figure('units','normalized','position',[.1 .1 .35 .3]);
 
 subplot(2,3,1); cla; hold on;
    x1 = nanmean(STSWD_summary.HDDM_vt.driftEEG_linear(idxSummaryIDs,1),2);
    y1 = nanmean(STSWD_summary.behav.EEGRT_linear(idxSummaryIDs,1),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'linear'}); ylabel({'RT';'linear'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg1 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    % add MRI
    x1 = nanmean(STSWD_summary.HDDM_vt.driftMRI_linear(idxSummaryIDs,1),2);
    y1 = nanmean(STSWD_summary.behav.MRIRT_linear(idxSummaryIDs,1),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');
    
 subplot(2,3,3); cla; hold on;
    x1 = nanmean(STSWD_summary.HDDM_vt.nondecisionEEG_linear(idxSummaryIDs,1),2);
    y1 = nanmean(STSWD_summary.behav.EEGRT_linear(idxSummaryIDs,1),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'NDT', 'linear'}); ylabel({'RT';'linear'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg2 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    % add MRI
    x1 = nanmean(STSWD_summary.HDDM_vt.nondecisionMRI_linear(idxSummaryIDs,1),2);
    y1 = nanmean(STSWD_summary.behav.MRIRT_linear(idxSummaryIDs,1),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');
    
 subplot(2,3,4); cla; hold on;
    x1 = nanmean(STSWD_summary.HDDM_vt.driftEEG_linear(idxSummaryIDs,1),2);
    y1 = nanmean(STSWD_summary.behav.EEGAcc_linear(idxSummaryIDs,1),2);    
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'Drift', 'linear'}); ylabel({'Accuracy';'linear'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg3 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    % add MRI
    x1 = nanmean(STSWD_summary.HDDM_vt.driftMRI_linear(idxSummaryIDs,1),2);
    y1 = nanmean(STSWD_summary.behav.MRIAcc_linear(idxSummaryIDs,1),2);    
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');
    
 subplot(2,3,6); cla; hold on;
    x1 = nanmean(STSWD_summary.HDDM_vt.nondecisionEEG_linear(idxSummaryIDs,1),2);
    y1 = nanmean(STSWD_summary.behav.EEGAcc_linear(idxSummaryIDs,1),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'k');
    xlabel({'NDT', 'linear'}); ylabel({'Accuracy';'linear'});
    y_ls = polyval(polyfit(x,y,1),x); y_ls = plot(x, y_ls, 'Color', [0 0 0], 'LineWidth', 1);
    [r, p] = corrcoef(x,y, 'rows', 'complete'); p = convertPtoExponential(p(2));
    lg4 = legend([y_ls], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]}, 'location', 'South'); legend('boxoff');
    % add MRI
    x1 = nanmean(STSWD_summary.HDDM_vt.nondecisionMRI_linear(idxSummaryIDs,1),2);
    y1 = nanmean(STSWD_summary.behav.MRIAcc_linear(idxSummaryIDs,1),2);
    x = squeeze(x1(~isnan(x1) & ~isnan(y1)));
    y = squeeze(y1(~isnan(x1) & ~isnan(y1)));
    scatter(x, y, 'filled', 'r');
    y_ls2 = polyval(polyfit(x,y,1),x); y_ls2 = plot(x, y_ls2, 'Color', [1 0 0], 'LineWidth', 1);
    [r2, p2] = corrcoef(x,y, 'rows', 'complete'); p2 = convertPtoExponential(p2(2));
    lg4 = legend([y_ls, y_ls2], {['r = ', num2str(round(r(2),2)), ', p = ' p{1}]; ...
        ['r = ', num2str(round(r2(2),2)), ', p = ' p2{1}]}, 'location', 'South'); legend('boxoff');

    set(findall(gcf,'-property','FontSize'),'FontSize',14)

figureName = 'C_RTACC_change_vt';

saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');
